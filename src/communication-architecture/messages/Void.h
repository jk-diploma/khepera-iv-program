//
// Created by jakub on 12/20/20.
//

#ifndef MESSAGE_BUS_LIBRARY_VOID_H
#define MESSAGE_BUS_LIBRARY_VOID_H


#include "ReturnData.h"

struct Void: ReturnData {

    ~Void() {}

    static const Void* shared();

private:
    static const Void* sharedInstance;

};


#endif //MESSAGE_BUS_LIBRARY_VOID_H
