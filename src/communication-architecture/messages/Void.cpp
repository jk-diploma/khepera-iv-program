//
// Created by jakub on 12/20/20.
//

#include "Void.h"

Void const *Void::sharedInstance = new Void();

Void const *Void::shared() {
    return sharedInstance;
}

