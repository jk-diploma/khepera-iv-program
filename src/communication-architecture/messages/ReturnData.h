//
// Created by jakub on 12/20/20.
//

#ifndef MESSAGE_BUS_LIBRARY_RETURNDATA_H
#define MESSAGE_BUS_LIBRARY_RETURNDATA_H


struct ReturnData {
    /**
     * Destructor created because using dynamic cast from this class to its derived instance.
     */
    virtual ~ReturnData() {}
};


#endif //MESSAGE_BUS_LIBRARY_RETURNDATA_H
