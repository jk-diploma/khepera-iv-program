//
// Created by jakub on 7/3/20.
//

#ifndef MESSAGE_BUS_LIBRARY_MESSAGE_H
#define MESSAGE_BUS_LIBRARY_MESSAGE_H

#include <iostream>

/**
 * Base class that represents Message used to communicate between layers of application using listeners.
 * IMPORTANT one message must be handle by exactly one listener. Listener is class extending MessageListener
 */
struct Message{
    /**
     * Destructor created because using dynamic cast from this class to its derived instance.
     */
    virtual ~Message() {}

};

#endif //MESSAGE_BUS_LIBRARY_MESSAGE_H
