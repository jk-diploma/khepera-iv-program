//
// Created by jakub on 9/30/20.
//

#include "MessageBus.h"

MessageBus *MessageBus::messageBus = NULL;

void MessageBus::unregisterMessageListener(AbstractMessageListener *listener)  {
    if(listener != NULL) {
        Logger::info<MessageBus>("Unregistering listener: " + listener->getIdentifier());
        unregisterMessageListener(listener->getIdentifier());
    }
}

void MessageBus::unregisterMessageListener(std::string identifier) {
    if(messageListeners.find(identifier) != messageListeners.end()) {
        delete messageListeners[identifier];
    }
    messageListeners.erase(identifier);
}





