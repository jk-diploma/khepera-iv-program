//
// Created by jakub on 7/3/20.
//

#ifndef MESSAGE_BUS_LIBRARY_ABSTRACTMESSAGELISTENER_H
#define MESSAGE_BUS_LIBRARY_ABSTRACTMESSAGELISTENER_H


#include "../messages/Message.h"
#include "../../utils/IdentifierGenerator.h"
#include "../messages/ReturnData.h"

/**
 * Represents listener to message.
 * It was created because MessageBus must have inside map non template class.
 * It's has only one derived class - MessageListener which final overrides onMessage
 * casting Message onto derived instance that current listener listen to.
 */
class AbstractMessageListener {
    std::string *identifier;
public:
    AbstractMessageListener(): identifier(NULL) {}
    virtual ~AbstractMessageListener() {
        delete identifier;
    }

    /**
     * Extended inside MessageListener to dynamic cast into properly derived instance of Message
     * @param message on which listener listen to.
     */
    virtual const ReturnData* onMessage(Message *message) = 0;

    std::string getIdentifier() {
        if(identifier == NULL) {
            identifier = new std::string(generateSimpleIdentifier());
        }
        return *identifier;
    }

};


#endif //MESSAGE_BUS_LIBRARY_ABSTRACTMESSAGELISTENER_H
