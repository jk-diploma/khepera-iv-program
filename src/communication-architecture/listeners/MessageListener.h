//
// Created by jakub on 7/3/20.
//

#ifndef MESSAGE_BUS_LIBRARY_MESSAGELISTENER_H
#define MESSAGE_BUS_LIBRARY_MESSAGELISTENER_H


#include "AbstractMessageListener.h"

/**
 * Base class for all listeneres in application.
 * Every listener extends only onMessage(T) because onMessage(Message) is final override.
 * @tparam T
 */
template<typename T>
class MessageListener: public AbstractMessageListener {
public:

    virtual ~MessageListener() {}

    /**
     * Event called when something send Message of current (T) type. Deleting pointer should be done manually
     * @param message sent message
     */
    virtual const ReturnData* onMessage(T *message) = 0;

    const ReturnData* onMessage(Message *message) {
        return this->onMessage(dynamic_cast<T *>(message));
    }
};


#endif //MESSAGE_BUS_LIBRARY_MESSAGELISTENER_H
