#ifndef MESSAGE_BUS_LIBRARY_MESSAGEBUS_H
#define MESSAGE_BUS_LIBRARY_MESSAGEBUS_H

#include <map>
#include <iostream>
#include "messages/Message.h"
#include "listeners/AbstractMessageListener.h"
#include "listeners/MessageListener.h"
#include "../utils/Logger.h"
#include <typeinfo>

class MessageBus {

    static MessageBus *messageBus;

    std::map<std::string, AbstractMessageListener* > messageListeners;

    MessageBus() {}

public:
    template<typename T>
    MessageListener<T>* registerListener(MessageListener<T> *listener);

    void unregisterMessageListener(std::string identifier);

    void unregisterMessageListener(AbstractMessageListener *listener);

    /**
     * TODO fill this
     * <b>IT DOES NOT DELETE RETURN DATA</b>
     * @tparam T
     * @tparam R
     * @param message
     * @return
     */
    template<typename T, typename R>
    const R* sendMessage(T message);

    static MessageBus &instance() {
        if (messageBus == NULL) {
            messageBus = new MessageBus();
        }
        return *messageBus;
    }

};

template<typename T>
MessageListener<T> *MessageBus::registerListener(MessageListener<T> *listener)  {
    std::string pointerStartingWith = "P";
    std::string className = typeid(T).name();
    Logger::info<MessageBus>("Registering listener on message: " + className);
    messageListeners[className] = listener;
    messageListeners[pointerStartingWith.append(className)] = listener;
    return listener;
}

template<typename T, typename R>
const R* MessageBus::sendMessage(T message) {
    std::string className = typeid(T).name();
    std::map<std::string, AbstractMessageListener*>::iterator listener = messageListeners.find(className);
    if(listener != messageListeners.end()) {
        Logger::debug<MessageBus>("Found listener for " + className + ". Sending message.");
        return dynamic_cast<const R *>(listener->second->onMessage(&message));
    } else {
        Logger::error<MessageBus>("Listener on class: " + className + " not found.");
        return NULL;
    }
    return NULL;
}

#endif //MESSAGE_BUS_LIBRARY_MESSAGEBUS_H