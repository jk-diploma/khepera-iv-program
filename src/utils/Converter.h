//
// Created by user on 3/3/21.
//

#ifndef MESSAGE_BUS_LIBRARY_CONVERTER_H
#define MESSAGE_BUS_LIBRARY_CONVERTER_H

#include <iostream>
#include <string>
#include <sstream>

template<typename T>
std::string toString(T any) {
    std::ostringstream convert;
    convert << any;
    return convert.str();
}

template<typename T>
T toT(std::string s) {
    T t;
    std::istringstream(s) >> t;
    return t;
}

#endif //MESSAGE_BUS_LIBRARY_CONVERTER_H
