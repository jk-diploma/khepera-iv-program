
#include <gcrypt.h>
#include "./Logger.h"
#include "../configs/Properties.h"
#include "../configs/Configuration.h"

#define LOG_LEVEL_KEY "LOG_LEVEL"
#define LOG_LEVEL_DEFAULT_VALUE "INFO"

#ifndef CONFIG_FILE_PATH
    #define CONFIG_FILE_PATH "./config.properties"
#endif

int Logger::logLevel = -1;

int Logger::getLogLevel() {
    if(logLevel == -1) {
        Properties prop = Configuration::loadConfigurationPropertiesFromFile(CONFIG_FILE_PATH);
        std::string val = prop.get(LOG_LEVEL_KEY, LOG_LEVEL_DEFAULT_VALUE);
        std::cout << "Log level is: " << val << std::endl;
        if(val == "DEBUG") {
            logLevel = 2;
        } else {
            logLevel = 1;
        }
    }
    return logLevel;
}