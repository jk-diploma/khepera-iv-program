//
// Created by root on 23.01.2021.
//

#include "LogMessage.h"
#include "./Converter.h"

std::string LogMessage::serialize() {
    return deviceIdentifier + ";" + toString(dataTime) + ";" + log;
}

TopicWildcard LogMessage::topic() {
    return TopicWildcard("/robots/+/log");
}

