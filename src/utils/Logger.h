//
// Created by jakub on 7/7/20.
//

#ifndef MESSAGE_BUS_LIBRARY_LOGGER_H
#define MESSAGE_BUS_LIBRARY_LOGGER_H
#include <string>
#include <typeinfo>
#include <iostream>
#include "../configs/Configuration.h"
#include "../mqtt-connection/manager/MqttManager.h"
#include "../mqtt-connection/communication/MqttCommunication.h"
#include "LogMessage.h"
#include "TimeUtils.h"

class Logger {

    static int logLevel;

    static int getLogLevel();

    inline static void  publishLog(std::string msg, bool sendToServer) {
        MqttCommunication* communication = &MqttManager::getMqttCommunication();
        if(MqttManager::isConnectedWithServer() && sendToServer && communication != NULL) {
            std::vector<std::string> topic;
            topic.push_back(MqttManager::getMqttDeviceIdentifier());
            MqttManager::getMqttCommunication().publishMessage(
                    new LogMessage(
                            MqttManager::getMqttDeviceIdentifier(),
                            TimeUtils::currentTime(),
                            msg
                    ),
                    topic
            );
        }
    }

public:

    template<typename T>
    static void info(std::string msg, bool sendToServer = true);

    template<typename T>
    static void debug(std::string msg, bool sendToServer = true);

    template<typename T>
    static void error(std::string msg, bool sendToServer = true);

};

template<typename T>
void Logger::info(std::string msg, bool sendToServer ) {
    if(getLogLevel() >= 1) {
        std::string className =  typeid(T).name();
        std::string log = "[INFO][" + className + "] " + msg;
        std::cout << log << std::endl;
        publishLog(log, sendToServer);
    }
}

template<typename T>
void Logger::debug(std::string msg, bool sendToServer) {
    if(getLogLevel() >= 2) {
        std::string className =  typeid(T).name();
        std::string log = "[DEBUG][" + className + "] " + msg;
        std::cout << log << std::endl;
        publishLog(log, sendToServer);
    }
}

template<typename T>
void Logger::error(std::string msg, bool sendToServer) {
    if(getLogLevel() >= 1) {
        std::string className =  typeid(T).name();
        std::string log = "[ERROR][" + className + "] " + msg;
        std::cout << log << std::endl;
        publishLog(log, sendToServer);
    }
}

#endif //MESSAGE_BUS_LIBRARY_LOGGER_H
