//
// Created by root on 23.01.2021.
//

#ifndef MESSAGE_BUS_LIBRARY_LOGMESSAGE_H
#define MESSAGE_BUS_LIBRARY_LOGMESSAGE_H

#include "../mqtt-connection/communication/MqttMessage.h"

class  LogMessage: public MqttMessage {

    std::string deviceIdentifier;
    Time dataTime;
    std::string log;

public:

    LogMessage(std::string deviceIdentifier, Time dataTime, std::string log): deviceIdentifier(deviceIdentifier), dataTime(dataTime), log(log) {}

    std::string serialize();

    TopicWildcard topic();
};

#endif //MESSAGE_BUS_LIBRARY_LOGMESSAGE_H
