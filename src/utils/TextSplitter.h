//
// Created by jakub on 1/18/21.
//

#include <vector>
#include <string>

class TextSplitter {
public:
    static std::vector<std::string> split(std::string s, std::string delimiter);
    static std::vector<std::string> split2(std::string s, char delimiter);
};
