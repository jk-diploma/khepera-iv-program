//
// Created by jakub on 9/27/20.
//
#include <string>
#include <random>
#include <stdlib.h>
#include <uuid/uuid.h>
#include "Converter.h"

std::string generateSimpleIdentifier() {
    uuid_t binuuid;
    uuid_generate_random(binuuid);
    char *uuid = (char*)malloc(37);
    uuid_unparse_upper(binuuid, uuid);
    return toString(uuid);
}

