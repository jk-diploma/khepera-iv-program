//
// Created by jakub on 1/18/21.
//


#include <vector>
#include "TextSplitter.h"
#include "Converter.h"

std::vector<std::string> TextSplitter::split(std::string s, std::string delimiter) {
    std::vector<std::string> parts;
    size_t pos;
    std::string token;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        parts.push_back(token);
        s.erase(0, pos + delimiter.length());
    }
    if(!s.empty()) {
        parts.push_back(s);
    }
    return parts;
}

std::vector<std::string> TextSplitter::split2(std::string s, char delimiter) {
    std::vector<std::string> parts;
    size_t pos;
    std::string token;
    while(s[0] == delimiter) {
        s.erase(0, 1);
    }
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        parts.push_back(token);
        s.erase(0, pos + toString(delimiter).length());
    }
    if(!s.empty()) {
        parts.push_back(s);
    }
    return parts;
}
