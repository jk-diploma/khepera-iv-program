//
// Created by jakub on 9/27/20.
//

#ifndef MESSAGE_BUS_LIBRARY_IDENTIFIERGENERATOR_H
#define MESSAGE_BUS_LIBRARY_IDENTIFIERGENERATOR_H

#include <string>

std::string generateSimpleIdentifier();

#endif //MESSAGE_BUS_LIBRARY_IDENTIFIERGENERATOR_H
