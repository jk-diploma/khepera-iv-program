//
// Created by jakub on 12/21/20.
//


#if !defined(MESSAGE_BUS_LIBRARY_KHEPERAHOLDER_H)
#define MESSAGE_BUS_LIBRARY_KHEPERAHOLDER_H

#include <knet.h>

class KheperaNamespace{};

namespace Khepera {

    knet_dev_t* getMicroControllerAccess();

};


#endif //MESSAGE_BUS_LIBRARY_KHEPERAHOLDER_H
