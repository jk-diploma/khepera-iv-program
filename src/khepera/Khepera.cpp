//
// Created by jakub on 12/21/20.
//

#include "../utils/Logger.h"
#include "Khepera.h"

#include <khepera/khepera.h>

knet_dev_t * dsPic = NULL;

knet_dev_t *Khepera::getMicroControllerAccess()  {
    if(dsPic == NULL) {
        if ( kh4_init(0,NULL) != 0)
        {
            Logger::error<KheperaNamespace>("Khepera robot could not be initialized");
            throw "Khepera robot could not be initialized";
        }
        dsPic = knet_open( "Khepera4:dsPic" , KNET_BUS_I2C , 0 , NULL );

        if ( dsPic == NULL)
        {
            Logger::error<KheperaNamespace>("Khepera could not initiate communication");
            throw "Khepera could not initiate communication";
        }
    }
    return dsPic;
}

