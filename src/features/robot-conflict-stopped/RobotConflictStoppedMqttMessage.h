//
// Created by jakub on 1/18/21.
//

#include <string>
#include "../../mqtt-connection/communication/MqttMessage.h"
#include "../../utils/Converter.h"
#include "../track-listener/TrackActionMessage.h"

#ifndef MESSAGE_BUS_LIBRARY_ROBOTSTOPPEDMQTTMESSAGE_H
#define MESSAGE_BUS_LIBRARY_ROBOTSTOPPEDMQTTMESSAGE_H

#endif //MESSAGE_BUS_LIBRARY_ROBOTSTOPPEDMQTTMESSAGE_H

class RobotConflictStoppedMqttMessage: public MqttMessage {
    std::string deviceIdentifier;
    Time dataTime;
    Crossid crossId;
public:

    RobotConflictStoppedMqttMessage(std::string deviceIdentifier, Time dataTime, Crossid crossId)
            : deviceIdentifier(deviceIdentifier), dataTime(dataTime), crossId(crossId) {}

    std::string serialize() {
        return deviceIdentifier + ";" + toString(dataTime) + ";" + toString(crossId);
    }

    TopicWildcard topic()  {
        return TopicWildcard("/conflicts/crossings/+/robots/+/+");
    }
};