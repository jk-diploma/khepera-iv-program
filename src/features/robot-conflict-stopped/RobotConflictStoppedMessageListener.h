//
// Created by jakub on 1/18/21.
//

#ifndef MESSAGE_BUS_LIBRARY_ROBOTSTOPPEDMESSAGESENDER_H
#define MESSAGE_BUS_LIBRARY_ROBOTSTOPPEDMESSAGESENDER_H


#include "../../communication-architecture/listeners/MessageListener.h"
#include "RobotConflictStoppedMessage.h"
#include "../../communication-architecture/messages/Void.h"

class RobotConflictStoppedMessageListener: public MessageListener<RobotConflictStoppedMessage> {
public:
    const Void *onMessage(RobotConflictStoppedMessage *message);
};


#endif //MESSAGE_BUS_LIBRARY_ROBOTSTOPPEDMESSAGESENDER_H
