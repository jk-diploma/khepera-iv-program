//
// Created by jakub on 1/18/21.
//

#include "RobotConflictStoppedMessageListener.h"
#include "../../communication-architecture/MessageBus.h"
#include "../crossing-actions-holder/CrossingActionsHolderMessage.h"
#include "../../mqtt-connection/manager/MqttManager.h"
#include "RobotConflictStoppedMqttMessage.h"
#include "../../utils/TimeUtils.h"
#include "../../utils/Converter.h"

const Void *RobotConflictStoppedMessageListener::onMessage(RobotConflictStoppedMessage *message) {
    Logger::debug<RobotConflictStoppedMessageListener>("Setting robot state as stopped.");
    const CrossingData *msg =
            MessageBus::instance().sendMessage<CrossingActionsHolderMessage, CrossingData>(CrossingActionsHolderMessage::getCurrent());
    if(msg != NULL) {
        const std::string& identifier = MqttManager::getMqttDeviceIdentifier();
        Time time = TimeUtils::currentTime();
        std::vector<std::string> topicParams;
        topicParams.push_back(toString(msg->crossId));
        topicParams.push_back(identifier);
        topicParams.push_back("STOP");
        MqttManager::getMqttCommunication()
                .publishMessage(
                        new RobotConflictStoppedMqttMessage(identifier, time, msg->crossId),
                        topicParams
                );
        delete msg;
    }
    return Void::shared();
}
