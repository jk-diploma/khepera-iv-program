//
// Created by user on 3/8/21.
//

#ifndef APPLICATION_ROBOTSTATEMESSAGELISTENER_H
#define APPLICATION_ROBOTSTATEMESSAGELISTENER_H

#include "../../communication-architecture/listeners/MessageListener.h"
#include "RobotStateMessage.h"

struct RobotStateReturnData: ReturnData {
private:
    RobotState robotState;
public:
    explicit RobotStateReturnData(RobotState robotState): robotState(robotState) {}

    inline RobotState getRobotState() const {
        return robotState;
    }
};

class RobotStateMessageListener: public MessageListener<RobotStateMessage> {
    RobotState currentRobotState;
public:
    virtual const RobotStateReturnData *onMessage(RobotStateMessage *message);

};


#endif //APPLICATION_ROBOTSTATEMESSAGELISTENER_H
