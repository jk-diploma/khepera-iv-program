//
// Created by user on 3/8/21.
//

#ifndef APPLICATION_ROBOTSTATEMESSAGE_H
#define APPLICATION_ROBOTSTATEMESSAGE_H

#include "../../communication-architecture/messages/Message.h"

enum RobotState {
    DRIVING,
    CONFLICTED,
    SHOULD_DRIVE,
    TRACK_END
};

class RobotStateMessage: public Message {
public:
    const bool settingData;
    const RobotState robotState;

    RobotStateMessage(bool settingData): settingData(settingData), robotState(DRIVING) {}; //not beauty solution
    RobotStateMessage(bool settingData, RobotState robotState): settingData(settingData), robotState(robotState) {};

    static RobotStateMessage getRobotState() {
        return RobotStateMessage(false);
    }

    static RobotStateMessage setDriving() {
        return RobotStateMessage(true, DRIVING);
    }

    static RobotStateMessage setConflicted() {
        return RobotStateMessage(true, CONFLICTED);
    }

    static RobotStateMessage setShouldDrive() {
        return RobotStateMessage(true, SHOULD_DRIVE);
    }

    static RobotStateMessage setTrackEnd() {
        return RobotStateMessage(true, TRACK_END);
    }
};

#endif //APPLICATION_ROBOTSTATEMESSAGE_H
