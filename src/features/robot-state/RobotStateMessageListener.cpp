//
// Created by user on 3/8/21.
//

#include "RobotStateMessageListener.h"


const RobotStateReturnData *RobotStateMessageListener::onMessage(RobotStateMessage *message) {
    if(message != NULL) {
        if(message->settingData) {
            this->currentRobotState = message->robotState;
            return new RobotStateReturnData(currentRobotState);
        } else {
            return new RobotStateReturnData(currentRobotState);
        }
    }
    return NULL;
}
