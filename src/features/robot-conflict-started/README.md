# Robot conflict started

## What it is for
When program detects possibility of collision with another robot this feature sends message to server with id of possible
vertex on which conflict would appear and robot identifier.