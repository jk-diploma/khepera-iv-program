//
// Created by jakub on 1/18/21.
//

#include "RobotConflictStartedMessageListener.h"
#include "../../mqtt-connection/manager/MqttManager.h"
#include "../../utils/TimeUtils.h"
#include "../../utils/Converter.h"
#include "RobotConflictStartedMqttMessage.h"
#include "../../communication-architecture/MessageBus.h"
#include "../crossing-actions-holder/CrossingActionsHolderMessage.h"



const Void *RobotConflictStartedMessageListener::onMessage(RobotConflictStartedMessage *message) {
    const CrossingData* msg =
            MessageBus::instance().sendMessage<CrossingActionsHolderMessage, CrossingData>(CrossingActionsHolderMessage::getNext());
    if(!this->isEndOfTrackReached(msg)) {
        const std::string& identifier = MqttManager::getMqttDeviceIdentifier();
        Time time = TimeUtils::currentTime();
        std::vector<std::string> topicParams;
        topicParams.push_back(toString(msg->crossId));
        topicParams.push_back(identifier);
        topicParams.push_back("START");
        MqttManager::getMqttCommunication().publishMessage(
            new RobotConflictStartedMqttMessage(identifier, time, msg->crossId),
            topicParams);
        delete msg;
    }
    return Void::shared();
}

bool RobotConflictStartedMessageListener::isEndOfTrackReached(const CrossingData *msg) {
    return msg == NULL;
}
