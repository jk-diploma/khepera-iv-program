//
// Created by jakub on 1/18/21.
//

#ifndef MESSAGE_BUS_LIBRARY_ROBOTSTARTEDMQTTMESSAGE_H
#define MESSAGE_BUS_LIBRARY_ROBOTSTARTEDMQTTMESSAGE_H


#include "../../mqtt-connection/communication/MqttMessage.h"
#include "../track-listener/TrackActionMessage.h"
#include "../../utils/Converter.h"

class RobotConflictStartedMqttMessage: public MqttMessage {
    std::string deviceIdentifier;
    Time dataTime;
    Crossid crossId;
public:

    RobotConflictStartedMqttMessage(std::string deviceIdentifier, Time dataTime, Crossid crossId)
        : deviceIdentifier(deviceIdentifier), dataTime(dataTime), crossId(crossId) {}

    std::string serialize() {
        return deviceIdentifier + ";" + toString(dataTime) + ";" + toString(crossId);
    }

    TopicWildcard topic() {
        return TopicWildcard("/conflicts/crossings/+/robots/+/+");
    }
};


#endif //MESSAGE_BUS_LIBRARY_ROBOTSTARTEDMQTTMESSAGE_H
