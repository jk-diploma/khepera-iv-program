//
// Created by jakub on 1/18/21.
//

#ifndef MESSAGE_BUS_LIBRARY_ROBOTSTARTEDMESSAGESENDER_H
#define MESSAGE_BUS_LIBRARY_ROBOTSTARTEDMESSAGESENDER_H


#include "../../communication-architecture/listeners/MessageListener.h"
#include "RobotConflictStartedMessage.h"
#include "../../communication-architecture/messages/Void.h"
#include "../crossing-actions-holder/CrossingActionsHolderMessage.h"

class RobotConflictStartedMessageListener: public MessageListener<RobotConflictStartedMessage> {

    bool isEndOfTrackReached(const CrossingData* msg);

public:
    const Void *onMessage(RobotConflictStartedMessage *message);

};


#endif //MESSAGE_BUS_LIBRARY_ROBOTSTARTEDMESSAGESENDER_H
