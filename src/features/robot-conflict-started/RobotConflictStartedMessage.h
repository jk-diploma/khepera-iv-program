//
// Created by jakub on 1/18/21.
//

#ifndef MESSAGE_BUS_LIBRARY_ROBOTSTARTEDMESSAGE2_H
#define MESSAGE_BUS_LIBRARY_ROBOTSTARTEDMESSAGE2_H


#include "../../communication-architecture/messages/Message.h"

class RobotConflictStartedMessage: public Message {};


#endif //MESSAGE_BUS_LIBRARY_ROBOTSTARTEDMESSAGE_H
