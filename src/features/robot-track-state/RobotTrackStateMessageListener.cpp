//
// Created by jakub on 1/18/21.
//

#include "RobotTrackStateMessageListener.h"
#include "../../utils/Logger.h"

const RobotTrackState *RobotTrackStateMessageListener::onMessage(RobotTrackStateMessage *message) {
    Logger::debug<RobotTrackStateMessageListener>("Getting or setting local robot track state.");
    switch (message->getAction()) {
        case ROBOT_TRACK_SET_START_STATE:
            trackStartedState = true;
            break;
        case ROBOT_TRACK_SET_STOP_STATE:
            trackStartedState = false;
            break;
    }
    return new RobotTrackState(trackStartedState);
}
