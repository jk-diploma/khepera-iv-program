//
// Created by jakub on 1/18/21.
//

#ifndef MESSAGE_BUS_LIBRARY_ROBOTTRACKSTATEMESSAGELISTENER_H
#define MESSAGE_BUS_LIBRARY_ROBOTTRACKSTATEMESSAGELISTENER_H


#include "RobotTrackStateMessage.h"
#include "../../communication-architecture/listeners/MessageListener.h"

struct RobotTrackState: ReturnData {
private:
    bool started;
public:
    explicit RobotTrackState(bool started): started(started) {};
    inline bool isTrackStarted() const {
        return started;
    }
};

class RobotTrackStateMessageListener: public MessageListener<RobotTrackStateMessage> {
    bool trackStartedState;
public:
    RobotTrackStateMessageListener() {
        trackStartedState = false;
    }
    const RobotTrackState *onMessage(RobotTrackStateMessage *message);
};


#endif //MESSAGE_BUS_LIBRARY_ROBOTTRACKSTATEMESSAGELISTENER_H
