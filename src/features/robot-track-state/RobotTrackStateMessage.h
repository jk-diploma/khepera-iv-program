//
// Created by jakub on 1/18/21.
//

#ifndef MESSAGE_BUS_LIBRARY_ROBOTTRACKSTATEMESSAGE_H
#define MESSAGE_BUS_LIBRARY_ROBOTTRACKSTATEMESSAGE_H

#include "../../communication-architecture/messages/Message.h"

enum RobotTrackStateAction {
    ROBOT_TRACK_CHECK_STATE,
    ROBOT_TRACK_SET_START_STATE,
    ROBOT_TRACK_SET_STOP_STATE
};

class RobotTrackStateMessage: public Message {
    RobotTrackStateAction action;
public:
    explicit RobotTrackStateMessage(RobotTrackStateAction action): action(action) {};
    inline RobotTrackStateAction getAction() {
        return action;
    }

    inline static RobotTrackStateMessage check() {
        return RobotTrackStateMessage(ROBOT_TRACK_CHECK_STATE);
    }

    inline static RobotTrackStateMessage start() {
        return RobotTrackStateMessage(ROBOT_TRACK_SET_START_STATE);
    }

    inline static RobotTrackStateMessage stop() {
        return RobotTrackStateMessage(ROBOT_TRACK_SET_STOP_STATE);
    }
};

#endif //MESSAGE_BUS_LIBRARY_ROBOTTRACKSTATEMESSAGE_H
