//
// Created by jakub on 12/20/20.
//

#ifndef MESSAGE_BUS_LIBRARY_FEATURE_H
#define MESSAGE_BUS_LIBRARY_FEATURE_H

class Feature {

public:

    //FIXME: ?
    virtual ~Feature() {}
    virtual void begin() = 0;

};

#endif //MESSAGE_BUS_LIBRARY_FEATURE_H
