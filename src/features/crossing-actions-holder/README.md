# Crossing actions holder

## What it is for
It is for holding actions related to crossing on line following mode. So when detected crossing lines it should call
this feature and it return what to do. Also it allows to set these actions.