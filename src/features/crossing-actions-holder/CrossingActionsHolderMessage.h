//
// Created by jakub on 1/18/21.
//

#ifndef MESSAGE_BUS_LIBRARY_CROSSINGACTIONSHOLDERMESSAGE_H
#define MESSAGE_BUS_LIBRARY_CROSSINGACTIONSHOLDERMESSAGE_H


#include <vector>
#include "../../communication-architecture/messages/Message.h"
#include "../track-listener/TrackActionMessage.h"

enum CrossingActionsOperation {
    GET_NEXT,
    POOL_NEXT,
    SET_CROSSING_ACTIONS,
    CURRENT
};

class CrossingActionsHolderMessage: public Message {

    CrossingActionsOperation operation;
    std::vector<CrossingData> crossingData;

    CrossingActionsHolderMessage(CrossingActionsOperation operation, std::vector<CrossingData> crossingData):
        operation(operation),
        crossingData(crossingData) {}

    CrossingActionsHolderMessage(CrossingActionsOperation operation):
            operation(operation) {}

public:

    CrossingActionsOperation getOperation() {
        return operation;
    }

    std::vector<CrossingData> getCrossingData() {
        return crossingData;
    }

    static CrossingActionsHolderMessage getNext() {
        return CrossingActionsHolderMessage(GET_NEXT);
    }

    static CrossingActionsHolderMessage poolNext() {
        return CrossingActionsHolderMessage(POOL_NEXT);
    }

    static CrossingActionsHolderMessage getCurrent() {
        return CrossingActionsHolderMessage(CURRENT);
    }

    static CrossingActionsHolderMessage setCrossingActions(std::vector<CrossingData> crossingData) {
        return CrossingActionsHolderMessage(SET_CROSSING_ACTIONS, crossingData);
    }

};

#endif //MESSAGE_BUS_LIBRARY_CROSSINGACTIONSHOLDERMESSAGE_H
