//
// Created by jakub on 1/18/21.
//

#include "CrossingActionsHolderMessageListener.h"
#include "../../utils/Logger.h"

const CrossingData *CrossingActionsHolderMessageListener::onMessage(CrossingActionsHolderMessage *message) {
    switch (message->getOperation()) {
        case GET_NEXT:
            Logger::debug<CrossingActionsHolderMessageListener>("Getting next crossing action");
            return crossingData.empty() ? NULL : new CrossingData(crossingData.at(0));
        case SET_CROSSING_ACTIONS:
            Logger::debug<CrossingActionsHolderMessageListener>("Setting crossing actions");
            crossingData = message->getCrossingData();
            return NULL;
        case CURRENT:
            return (lastPooled != NULL) ? new CrossingData(*lastPooled) : NULL;
        case POOL_NEXT:
            Logger::debug<CrossingActionsHolderMessageListener>("Polling next crossing action");
            if(crossingData.empty()) {
                return NULL;
            }
            CrossingData* data = new CrossingData(crossingData.at(0));
            lastPooled = &crossingData.at(0);
            crossingData.erase(crossingData.begin());
            return data;
    }
}
