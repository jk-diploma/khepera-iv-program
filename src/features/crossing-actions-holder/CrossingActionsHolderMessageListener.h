//
// Created by jakub on 1/18/21.
//

#ifndef MESSAGE_BUS_LIBRARY_CROSSINGACTIONSHOLDERMESSAGELISTENER_H
#define MESSAGE_BUS_LIBRARY_CROSSINGACTIONSHOLDERMESSAGELISTENER_H


#include "../../communication-architecture/listeners/MessageListener.h"
#include "CrossingActionsHolderMessage.h"

class CrossingActionsHolderMessageListener: public MessageListener<CrossingActionsHolderMessage> {

    CrossingData *lastPooled;
    std::vector<CrossingData> crossingData;

public:
    const CrossingData *onMessage(CrossingActionsHolderMessage *message);

};


#endif //MESSAGE_BUS_LIBRARY_CROSSINGACTIONSHOLDERMESSAGELISTENER_H
