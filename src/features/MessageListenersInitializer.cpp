//
// Created by jakub on 1/20/21.
//

#include "MessageListenersInitializer.h"
#include "../communication-architecture/MessageBus.h"
#include "battery-info/BatteryPercentageMessageListener.h"
#include "heartbeat/BasicInfoDataMessageListener.h"
#include "track-listener/TrackActionMessageListener.h"
#include "../mqtt-connection/manager/MqttManager.h"
#include "track-listener/TrackStartSubscriber.h"
#include "track-listener/TrackStopSubscriber.h"
#include "crossing-actions-holder/CrossingActionsHolderMessageListener.h"
#include "detect-collision/DetectCollisionMessageListener.h"
#include "line-follower/LineFollowMessageListener.h"
#include "robot-drive-loop/RobotDriveLoopMessageListener.h"
#include "robot-conflict-started/RobotConflictStartedMessageListener.h"
#include "robot-conflict-stopped/RobotConflictStoppedMessageListener.h"
#include "robot-track-state/RobotTrackStateMessageListener.h"

MessageListenersInitializer::~MessageListenersInitializer() {
    MessageBus::instance().unregisterMessageListener(batteryMessageListener);
    MessageBus::instance().unregisterMessageListener(basicInfoDataMessageListener);
    MessageBus::instance().unregisterMessageListener(trackActionMessageListener);
    MessageBus::instance().unregisterMessageListener(crossingActionsHolderMessageListener);
    MessageBus::instance().unregisterMessageListener(detectCollisionMessageListener);
    MessageBus::instance().unregisterMessageListener(lineFollowMessageListener);
    MessageBus::instance().unregisterMessageListener(robotDriveLoopMessageListener);
    MessageBus::instance().unregisterMessageListener(robotConflictStartedMessageListener);
    MessageBus::instance().unregisterMessageListener(robotConflictStoppedMessageListener);
    MessageBus::instance().unregisterMessageListener(robotTrackStateMessageListener);
    MessageBus::instance().unregisterMessageListener(robotStateMessageListener);

    MqttManager::getMqttCommunication().unsubscribe(startTrackSubscriber);
    MqttManager::getMqttCommunication().unsubscribe(stopTrackSubscriber);
    MqttManager::getMqttCommunication().unsubscribe(robotShouldDriveSubscriber);
    delete startTrackSubscriber;
    delete stopTrackSubscriber;
    delete robotShouldDriveSubscriber;
}

void MessageListenersInitializer::begin() {
    batteryMessageListener = MessageBus::instance().registerListener(new BatteryPercentageMessageListener());
    basicInfoDataMessageListener = MessageBus::instance().registerListener(new BasicInfoDataMessageListener());
    trackActionMessageListener = MessageBus::instance().registerListener(new TrackActionMessageListener());
    crossingActionsHolderMessageListener = MessageBus::instance().registerListener(new CrossingActionsHolderMessageListener());
    detectCollisionMessageListener = MessageBus::instance().registerListener(new DetectCollisionMessageListener());
    lineFollowMessageListener = MessageBus::instance().registerListener(new LineFollowMessageListener());
    robotDriveLoopMessageListener = MessageBus::instance().registerListener(new RobotDriveLoopMessageListener());
    robotConflictStartedMessageListener = MessageBus::instance().registerListener(new RobotConflictStartedMessageListener());
    robotConflictStoppedMessageListener = MessageBus::instance().registerListener(new RobotConflictStoppedMessageListener());
    robotTrackStateMessageListener = MessageBus::instance().registerListener(new RobotTrackStateMessageListener());
    robotStateMessageListener = MessageBus::instance().registerListener(new RobotStateMessageListener());

    startTrackSubscriber = MqttManager::getMqttCommunication().subscribe(new TrackStartSubscriber());
    stopTrackSubscriber = MqttManager::getMqttCommunication().subscribe(new TrackStopSubscriber());
    robotShouldDriveSubscriber = MqttManager::getMqttCommunication().subscribe(new RobotShouldDriveSubscriber());
}
