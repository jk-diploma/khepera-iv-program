//
// Created by jakub on 1/19/21.
//

#include "DetectCollisionMessageListener.h"
#include "../../utils/Logger.h"

const CollisionDetectedStatus *DetectCollisionMessageListener::onMessage(DetectCollisionMessage *message) {
    //TODO: less important but it should be done (if time would allow this)
    Logger::debug<DetectCollisionMessageListener>("Checking if robot has probability of collision with another object");
    return new CollisionDetectedStatus(false);
}
