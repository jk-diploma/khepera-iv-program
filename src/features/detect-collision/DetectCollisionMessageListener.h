//
// Created by jakub on 1/19/21.
//

#ifndef MESSAGE_BUS_LIBRARY_DETECTCOLLISIONMESSAGELISTENER_H
#define MESSAGE_BUS_LIBRARY_DETECTCOLLISIONMESSAGELISTENER_H


#include "../../communication-architecture/listeners/MessageListener.h"
#include "DetectCollisionMessage.h"

struct CollisionDetectedStatus: public ReturnData {
    bool collisionDetected;
    CollisionDetectedStatus(bool collisionDetected): collisionDetected(collisionDetected) {}
};

class DetectCollisionMessageListener: public MessageListener<DetectCollisionMessage> {
public:
    const CollisionDetectedStatus *onMessage(DetectCollisionMessage *message);
};


#endif //MESSAGE_BUS_LIBRARY_DETECTCOLLISIONMESSAGELISTENER_H
