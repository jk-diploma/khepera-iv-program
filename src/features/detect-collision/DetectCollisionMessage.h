//
// Created by jakub on 1/18/21.
//

#ifndef MESSAGE_BUS_LIBRARY_DETECTCOLLISIONMESSAGE_H
#define MESSAGE_BUS_LIBRARY_DETECTCOLLISIONMESSAGE_H

#include "../../communication-architecture/messages/Message.h"

class DetectCollisionMessage: public Message {};

#endif //MESSAGE_BUS_LIBRARY_DETECTCOLLISIONMESSAGE_H
