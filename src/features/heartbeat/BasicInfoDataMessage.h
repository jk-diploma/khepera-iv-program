//
// Created by jakub on 9/30/20.
//

#ifndef MESSAGE_BUS_LIBRARY_BASICINFODATAMESSAGE_H
#define MESSAGE_BUS_LIBRARY_BASICINFODATAMESSAGE_H

#include "../../communication-architecture/messages/Message.h"

struct BasicInfoDataMessage: public Message{

    const int batteryPercentage;

    BasicInfoDataMessage(int batteryPercentage): batteryPercentage(batteryPercentage) {}
    ~BasicInfoDataMessage() {

    }

};

#endif //MESSAGE_BUS_LIBRARY_BASICINFODATAMESSAGE_H
