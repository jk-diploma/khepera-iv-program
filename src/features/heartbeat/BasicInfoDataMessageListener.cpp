//
// Created by jakub on 9/30/20.
//

#include "BasicInfoDataMessageListener.h"
#include "../../utils/Logger.h"
#include "../../mqtt-connection/manager/MqttManager.h"
#include "BasicInfoDataMqttMessage.h"
#include "../../utils/TimeUtils.h"
#include "../../utils/Converter.h"

Void* BasicInfoDataMessageListener::onMessage(BasicInfoDataMessage *message) {
    Logger::info<BasicInfoDataMessageListener>("Sending robot basic info");
    std::cout << toString(TimeUtils::currentTime()) << std::endl;
    MqttMessage *mqttMessage = new BasicInfoDataMqttMessage(MqttManager::getMqttDeviceIdentifier(), TimeUtils::currentTime(), message->batteryPercentage);
    std::vector<std::string> topic;
    topic.push_back(MqttManager::getMqttDeviceIdentifier());
    MqttManager::getMqttCommunication().publishMessage(mqttMessage, topic);
    return NULL;
}

BasicInfoDataMessageListener::~BasicInfoDataMessageListener() {

}
