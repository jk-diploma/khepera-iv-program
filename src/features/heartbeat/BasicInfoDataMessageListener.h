//
// Created by jakub on 9/30/20.
//

#ifndef MESSAGE_BUS_LIBRARY_BASICINFODATAMESSAGELISTENER_H
#define MESSAGE_BUS_LIBRARY_BASICINFODATAMESSAGELISTENER_H


#include "../../communication-architecture/listeners/MessageListener.h"
#include "BasicInfoDataMessage.h"
#include "../../communication-architecture/messages/Void.h"

class BasicInfoDataMessageListener: public MessageListener<BasicInfoDataMessage> {
public:
    ~BasicInfoDataMessageListener();

    Void* onMessage(BasicInfoDataMessage *message);
};


#endif //MESSAGE_BUS_LIBRARY_BASICINFODATAMESSAGELISTENER_H
