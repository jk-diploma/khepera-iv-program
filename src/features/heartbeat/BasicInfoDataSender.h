//
// Created by jakub on 9/30/20.
//

#ifndef MESSAGE_BUS_LIBRARY_BASICINFODATASENDER_H
#define MESSAGE_BUS_LIBRARY_BASICINFODATASENDER_H

#include "../../threading/BaseThread.h"

class BasicInfoDataSender: public BaseThread {

public:
    void execute();

};


#endif //MESSAGE_BUS_LIBRARY_BASICINFODATASENDER_H
