//
// Created by jakub on 9/30/20.
//

#include "BasicInfoDataMqttMessage.h"
#include "../../utils/Converter.h"

std::string BasicInfoDataMqttMessage::serialize() {
    return uuid + ";" + toString(dataTime) + ";" + toString(remainBatteryMinutes);
}

TopicWildcard BasicInfoDataMqttMessage::topic() {
    return TopicWildcard("/robots/+/heartbeat");
}
