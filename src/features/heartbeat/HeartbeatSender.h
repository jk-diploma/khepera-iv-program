//
// Created by jakub on 12/20/20.
//

#ifndef MESSAGE_BUS_LIBRARY_HEARTBEATSENDER_H
#define MESSAGE_BUS_LIBRARY_HEARTBEATSENDER_H

#ifndef CONFIG_FILE_PATH
    #define CONFIG_FILE_PATH "./config.properties"
#endif

#include "../../threading/ThreadExecutor.h"
#include "../../communication-architecture/listeners/MessageListener.h"
#include "BasicInfoDataMessage.h"
#include "../Feature.h"
#include "../../communication-architecture/messages/Void.h"
#include "../../utils/Converter.h"
#include <iostream>

#define SEND_DATA_PERIOD_IN_SECONDS_KEY "SEND_DATA_PERIOD_IN_SECONDS"
#define DEFAULT_SEND_DATA_PERIOD_IN_SECONDS_VALUE "5"


class HeartbeatSender : public Feature {

    int frequency;
    ThreadExecutor *thread;

public:

    HeartbeatSender(): thread(NULL) {
        std::string val = Configuration::loadConfigurationPropertiesFromFile(CONFIG_FILE_PATH)
                .get(SEND_DATA_PERIOD_IN_SECONDS_KEY, DEFAULT_SEND_DATA_PERIOD_IN_SECONDS_VALUE);
        this->frequency = toT<int>(val);
        std::cout << "Heartbeat frequency: " << this->frequency << std::endl;
    }

    ~HeartbeatSender();

    void begin();

};


#endif //MESSAGE_BUS_LIBRARY_HEARTBEATSENDER_H
