//
// Created by jakub on 9/30/20.
//

#include "BasicInfoDataSender.h"
#include "../../utils/Logger.h"
#include "../../communication-architecture/MessageBus.h"
#include "BasicInfoDataMessage.h"
#include "../../communication-architecture/messages/Void.h"
#include "../battery-info/BatteryPercentageMessage.h"
#include "../battery-info/BatteryPercentageData.h"

void BasicInfoDataSender::execute() {
    Logger::info<BasicInfoDataSender>("Sending message with robot basic info by mqtt");
    const BatteryPercentageData* percentage = MessageBus::instance().sendMessage<BatteryPercentageMessage, BatteryPercentageData>(BatteryPercentageMessage());
    MessageBus::instance().sendMessage<BasicInfoDataMessage, Void>(BasicInfoDataMessage(percentage->batteryPercentage));
    delete percentage;
}
