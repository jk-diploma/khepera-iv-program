# Heartbeat

## What it is for
It is cyclic thread which at given rate (defined in config.properties) sends heartbeat to server. Heartbeat contains
robot identifier (specified in config.properties or random uuid if not set) and battery percentage.