//
// Created by jakub on 9/30/20.
//

#ifndef MESSAGE_BUS_LIBRARY_BASICINFODATAMQTTMESSAGE_H
#define MESSAGE_BUS_LIBRARY_BASICINFODATAMQTTMESSAGE_H


#include "../../mqtt-connection/communication/MqttMessage.h"

class BasicInfoDataMqttMessage: public MqttMessage {
    const std::string uuid;
    const Time dataTime;
    const int remainBatteryMinutes;
public:

    BasicInfoDataMqttMessage(std::string uuid, Time dataTime, int remainBatteryMinutes): uuid(uuid), dataTime(dataTime), remainBatteryMinutes(remainBatteryMinutes) {}

    std::string serialize();

    TopicWildcard topic();
};


#endif //MESSAGE_BUS_LIBRARY_BASICINFODATAMQTTMESSAGE_H
