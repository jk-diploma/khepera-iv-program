//
// Created by jakub on 12/20/20.
//


#include "HeartbeatSender.h"
#include "BasicInfoDataSender.h"
#include "../../communication-architecture/MessageBus.h"
#include "BasicInfoDataMessageListener.h"

HeartbeatSender::~HeartbeatSender() {
    thread->stop();
}

void HeartbeatSender::begin() {
    thread = ThreadExecutor::newScheduledThreadExecutor(new BasicInfoDataSender(), 0, frequency);
    thread->execute();
}
