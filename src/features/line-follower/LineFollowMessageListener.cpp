//
// Created by jakub on 1/20/21.
//

#include "LineFollowMessageListener.h"
#include "../../communication-architecture/MessageBus.h"
#include "../crossing-actions-holder/CrossingActionsHolderMessage.h"
#include "../../configs/Configuration.h"
#include "../../configs/Properties.h"
#include "../../khepera/Khepera.h"
#include "../../utils/Converter.h"
#include "../robot-state/RobotStateMessageListener.h"
#include "../track-listener/TrackActionMessageListener.h"
#include <khepera/khepera.h>

#ifndef CONFIG_FILE_PATH
    #define CONFIG_FILE_PATH "./config.properties"
#endif

#define FRONT_LEFT_SENSOR_ID 9
#define FRONT_RIGHT_SENSOR_ID 10
#define LEFT_SENSOR_ID 8
#define RIGHT_SENSOR_ID 11

#define LINE_COLOR_LOW_KEY "LINE_COLOR_LOW_RANGE"
#define LINE_COLOR_LOW_DEFAULT_VALUE 0
#define LINE_COLOR_HIGH_KEY "LINE_COLOR_HIGH_RANGE"
#define LINE_COLOR_HIGH_DEFAULT_VALUE 800

#define ROBOT_SPEED_KEY "ROBOT_SPEED"
#define ROBOT_SPEED_DEFAULT_VALUE 90
#define ROBOT_ROTATION_SPEED_KEY "ROBOT_ROTATION_SPEED"
#define ROBOT_ROTATION_SPEED_DEFAULT_VALUE 140

#define SECONDS_TO_MICROSECONDS_MULTIPLIER 1000000

#define LEFT_ROTATE -1
#define RIGHT_ROTATE 1

LineFollowMessageListener::LineFollowMessageListener() {
    this->leftSensorValue = 0;
    this->rightSensorValue = 0;
    Properties properties = Configuration::loadConfigurationPropertiesFromFile(CONFIG_FILE_PATH);
    this->lineLowerRange = properties.get(LINE_COLOR_LOW_KEY, LINE_COLOR_LOW_DEFAULT_VALUE);
    this->lineHigherRange = properties.get(LINE_COLOR_HIGH_KEY, LINE_COLOR_HIGH_DEFAULT_VALUE);
    this->speed = properties.get(ROBOT_SPEED_KEY, ROBOT_SPEED_DEFAULT_VALUE);
    this->rotationSpeed = properties.get(ROBOT_ROTATION_SPEED_KEY, ROBOT_ROTATION_SPEED_DEFAULT_VALUE);
    Logger::info<LineFollowMessageListener>("Following line properties:");
    Logger::info<LineFollowMessageListener>("LINE_COLOR_LOW_RANGE=[" + toString(this->lineLowerRange) + "]");
    Logger::info<LineFollowMessageListener>("LINE_COLOR_HIGH_RANGE=[" + toString(this->lineHigherRange) + "]");
    Logger::info<LineFollowMessageListener>("ROBOT_SPEED=[" + toString(this->speed) + "]");
    Logger::info<LineFollowMessageListener>("ROBOT_ROTATION_SPEED=[" + toString(this->rotationSpeed) + "]");
}

const Void *LineFollowMessageListener::onMessage(LineFollowMessage *message) {
    Logger::debug<LineFollowMessageListener>("Following line...");
    const CrossingData* anyExists =
            MessageBus::instance().sendMessage<CrossingActionsHolderMessage, CrossingData>(CrossingActionsHolderMessage::getNext());
    if(this->crossDetected()) {
        const CrossingData* nextVertex =
                MessageBus::instance().sendMessage<CrossingActionsHolderMessage, CrossingData>(CrossingActionsHolderMessage::poolNext());
        if(nextVertex != NULL) {
            Logger::debug<LineFollowMessageListener>("Cross detected, preparing to perform cross action.");
            CrossingAction action = nextVertex->action;
            CrossingType type = nextVertex->type;
            this->onCrossingAction(action, type);
        } else {
            //doesn't suppose to happen
            Logger::debug<LineFollowMessageListener>("Cross detected, but robot hasn't any cross action assigned.");
        }
        delete nextVertex;
    } else {
//        Logger::debug<LineFollowMessageListener>("Cross not detected, following line.");
        this->drive();
    }
    return Void::shared();
}

void LineFollowMessageListener::onCrossingAction(CrossingAction crossingAction, CrossingType type) {
    if(type == CT_END) {
        this->drive();
        usleep(4 * SECONDS_TO_MICROSECONDS_MULTIPLIER);
        this->onEndTrack();
        return;
    }
    switch(crossingAction) {
        case FORWARD:
            Logger::debug<LineFollowMessageListener>("On crossing action: FORWARD");
            this->onForward();
            break;
        case TURN_RIGHT:
            Logger::debug<LineFollowMessageListener>("On crossing action: RIGHT");
            this->onTurnRight();
            break;
        case TURN_LEFT:
            Logger::debug<LineFollowMessageListener>("On crossing action: LEFT");
            this->onTurnLeft();
            break;
    }
    if(type == CT_COLLECT_POINT) {
        this->onCollectPoint();
    }
}

void LineFollowMessageListener::onForward() {
    bool crossEnd = false;
    int leftMotorSpeed = this->speed;
    int rightMotorSpeed = this->speed;
    kh4_set_speed(leftMotorSpeed, rightMotorSpeed, Khepera::getMicroControllerAccess());
    usleep(2 * SECONDS_TO_MICROSECONDS_MULTIPLIER);

    char buffer[256];
    while(!crossEnd) {
        kh4_proximity_ir(buffer, Khepera::getMicroControllerAccess());
        this->leftSensorValue = this->getIRForSensor(FRONT_LEFT_SENSOR_ID, buffer);
        this->rightSensorValue = this->getIRForSensor(FRONT_RIGHT_SENSOR_ID, buffer);
        if(this->inLineRange(this->leftSensorValue) && this->inLineRange(this->rightSensorValue)) {
            leftMotorSpeed = this->speed;
            rightMotorSpeed = this->speed;
            crossEnd = true;
        } else if(this->inLineRange(this->leftSensorValue) && !this->inLineRange(this->rightSensorValue)) {
            leftMotorSpeed = this->notRotateMotorSpeed();
            rightMotorSpeed = this->rotationSpeed;
        } else if(!this->inLineRange(this->leftSensorValue) && this->inLineRange(this->rightSensorValue)) {
            leftMotorSpeed = this->rotationSpeed;
            rightMotorSpeed = this->notRotateMotorSpeed();
        } else {

            leftMotorSpeed = -this->notRotateMotorSpeed();
            rightMotorSpeed = this->notRotateMotorSpeed();
        }
        kh4_set_speed(leftMotorSpeed, rightMotorSpeed, Khepera::getMicroControllerAccess());
    }
}

void LineFollowMessageListener::onTurnRight() {
    char buffer[256];
    int leftMotorSpeed  = this->speed;
    int rightMotorSpeed = this->speed;
    kh4_set_speed(leftMotorSpeed, rightMotorSpeed, Khepera::getMicroControllerAccess());
    usleep(0.5 * SECONDS_TO_MICROSECONDS_MULTIPLIER);
    this->rotateRobot(RIGHT_ROTATE);
    this->onForward();
    leftMotorSpeed  = this->speed;
    rightMotorSpeed = this->speed;
    kh4_set_speed(leftMotorSpeed, rightMotorSpeed, Khepera::getMicroControllerAccess());
}

void LineFollowMessageListener::rotateRobot(int rotation) {
    int leftMotorSpeed = rotation * this->notRotateMotorSpeed();
    int rightMotorSpeed = -rotation * this->notRotateMotorSpeed();
    kh4_set_speed(leftMotorSpeed, rightMotorSpeed, Khepera::getMicroControllerAccess());
    usleep(2.6 * SECONDS_TO_MICROSECONDS_MULTIPLIER);
}

void LineFollowMessageListener::onTurnLeft() {
    char buffer[256];
    int leftMotorSpeed  = this->speed;
    int rightMotorSpeed = this->speed;
    kh4_set_speed(leftMotorSpeed, rightMotorSpeed, Khepera::getMicroControllerAccess());
    usleep(0.5 * SECONDS_TO_MICROSECONDS_MULTIPLIER);
    this->rotateRobot(LEFT_ROTATE);
    this->onForward();
    leftMotorSpeed  = this->speed;
    rightMotorSpeed = this->speed;
    kh4_set_speed(leftMotorSpeed, rightMotorSpeed, Khepera::getMicroControllerAccess());
}

void LineFollowMessageListener::onCollectPoint() {
    kh4_set_speed(this->speed, this->speed, Khepera::getMicroControllerAccess());
    usleep(1 * SECONDS_TO_MICROSECONDS_MULTIPLIER);
    this->rotateRobot(RIGHT_ROTATE);
    this->rotateRobot(RIGHT_ROTATE);
    kh4_set_speed(0, 0, Khepera::getMicroControllerAccess());
    usleep(5 * SECONDS_TO_MICROSECONDS_MULTIPLIER);
}

bool LineFollowMessageListener::crossDetected() {
    char buffer[256];
    kh4_proximity_ir(buffer, Khepera::getMicroControllerAccess());
    this->leftSensorValue = this->getIRForSensor(LEFT_SENSOR_ID, buffer);
    this->rightSensorValue = this->getIRForSensor(RIGHT_SENSOR_ID, buffer);
    return this->inLineRange(this->leftSensorValue) || this->inLineRange(this->rightSensorValue);
}

int LineFollowMessageListener::getIRForSensor(int sensorId, char *buffer) {
    return buffer[sensorId * 2] | buffer[sensorId * 2 + 1] << 8;
}

void LineFollowMessageListener::drive() {
    int leftMotorSpeed = this->speed;
    int rightMotorSpeed = this->speed;
    char buffer[256];
    kh4_proximity_ir(buffer, Khepera::getMicroControllerAccess());
    this->leftSensorValue = this->getIRForSensor(FRONT_LEFT_SENSOR_ID, buffer);
    this->rightSensorValue = this->getIRForSensor(FRONT_RIGHT_SENSOR_ID, buffer);
    if(this->inLineRange(this->leftSensorValue) && this->inLineRange(this->rightSensorValue)) {
        leftMotorSpeed = this->speed;
        rightMotorSpeed = this->speed;
    } else if(this->inLineRange(this->leftSensorValue) && !this->inLineRange(this->rightSensorValue)) {
        leftMotorSpeed = this->notRotateMotorSpeed();
        rightMotorSpeed = this->rotationSpeed;
    } else if(!this->inLineRange(this->leftSensorValue) && this->inLineRange(this->rightSensorValue)) {
        leftMotorSpeed = this->rotationSpeed;
        rightMotorSpeed = this->notRotateMotorSpeed();
    } else {
        leftMotorSpeed = -this->notRotateMotorSpeed();
        rightMotorSpeed = this->notRotateMotorSpeed();
    }
    kh4_set_speed(leftMotorSpeed, rightMotorSpeed, Khepera::getMicroControllerAccess());
    usleep(0.1 * SECONDS_TO_MICROSECONDS_MULTIPLIER);
}

void LineFollowMessageListener::onEndTrack() {
    Logger::debug<LineFollowMessageListener>("Ending track...");
    delete MessageBus::instance().sendMessage<RobotStateMessage, RobotStateReturnData>(RobotStateMessage::setTrackEnd());
    MessageBus::instance().sendMessage<TrackActionMessage, Void>(TrackActionMessage::stop());
}

