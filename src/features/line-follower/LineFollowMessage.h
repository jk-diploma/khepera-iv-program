//
// Created by jakub on 1/18/21.
//

#ifndef MESSAGE_BUS_LIBRARY_LINEFOLLOWMESSAGE_H
#define MESSAGE_BUS_LIBRARY_LINEFOLLOWMESSAGE_H

#include "../../communication-architecture/messages/Message.h"

class LineFollowMessage: public Message {};

#endif //MESSAGE_BUS_LIBRARY_LINEFOLLOWMESSAGE_H
