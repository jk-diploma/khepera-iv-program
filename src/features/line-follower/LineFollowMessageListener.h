//
// Created by jakub on 1/20/21.
//

#ifndef MESSAGE_BUS_LIBRARY_LINEFOLLOWMESSAGELISTENER_H
#define MESSAGE_BUS_LIBRARY_LINEFOLLOWMESSAGELISTENER_H


#include "../../communication-architecture/listeners/MessageListener.h"
#include "LineFollowMessage.h"
#include "../../communication-architecture/messages/Void.h"
#include "../track-listener/TrackActionMessage.h"

class LineFollowMessageListener: public MessageListener<LineFollowMessage> {

    int leftSensorValue;
    int rightSensorValue;

    int speed;
    int rotationSpeed;

    int lineLowerRange;
    int lineHigherRange;

    int getIRForSensor(int sensorId, char *buffer);

    void onCrossingAction(CrossingAction crossingAction, CrossingType type);
    void onForward();
    void onTurnRight();
    void onTurnLeft();
    void onCollectPoint();

    bool crossDetected();

    inline bool inLineRange(int val) {
        return val >= lineLowerRange && val <= lineHigherRange;
    }

    void drive();

    inline int notRotateMotorSpeed() {
        return this->rotationSpeed - this->speed;
    }

    void onEndTrack();

    void rotateRobot(int rotation);

public:
    LineFollowMessageListener();
    const Void *onMessage(LineFollowMessage *message);
};


#endif //MESSAGE_BUS_LIBRARY_LINEFOLLOWMESSAGELISTENER_H
