//
// Created by jakub on 12/20/20.
//

#include "TrackStopSubscriber.h"
#include "../../mqtt-connection/manager/MqttManager.h"
#include "../../utils/Logger.h"
#include "../../communication-architecture/MessageBus.h"
#include "TrackActionMessage.h"
#include "../../communication-architecture/messages/Void.h"

void TrackStopSubscriber::onMessage(TopicPath topic, std::string message) {
    Logger::info<TrackStopSubscriber>("Received stop track message");
    MessageBus::instance().sendMessage<TrackActionMessage, Void>(TrackActionMessage::stop());
}

std::vector<TopicWildcard> TrackStopSubscriber::topics() {
    std::vector<TopicWildcard> topic;
    topic.push_back(TopicWildcard("/robots/" + MqttManager::getMqttDeviceIdentifier() + "/stop-track"));
    return topic;
}
