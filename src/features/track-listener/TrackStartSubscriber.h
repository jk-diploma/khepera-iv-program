//
// Created by jakub on 12/20/20.
//

#ifndef MESSAGE_BUS_LIBRARY_TRACKSTARTSUBSCRIBER_H
#define MESSAGE_BUS_LIBRARY_TRACKSTARTSUBSCRIBER_H


#include "../../mqtt-connection/communication/Subscriber.h"

class TrackStartSubscriber: public Subscriber {
public:
    void onMessage(TopicPath topic, std::string message);

    std::vector<TopicWildcard> topics();

};


#endif //MESSAGE_BUS_LIBRARY_TRACKSTARTSUBSCRIBER_H
