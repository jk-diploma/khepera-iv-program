//
// Created by jakub on 12/20/20.
//

#include "TrackActionMessage.h"
#include "../../utils/TextSplitter.h"
#include "../../utils/Converter.h"

TrackActionMessage TrackActionMessage::start(std::string crossingActions) {
    std::vector<CrossingData> crossingData;
    std::vector<std::string> allVertexes = TextSplitter::split(crossingActions, "-");
    for(int i = 0; i < allVertexes.size(); i++) {
        std::string  singleVertex = allVertexes[i];
        std::vector<std::string> crossIdAndAction = TextSplitter::split(singleVertex, ":");
        Crossid crossId = toT<int>(crossIdAndAction[0]);
        char action = crossIdAndAction[1].at(0);
        CrossingType type = CT_NONE;
        if(crossIdAndAction[2].at(0) == 'C') {
            type = CT_COLLECT_POINT;
        } else if(crossIdAndAction[2].at(0) == 'S') {
            type = CT_START;
        } else if(crossIdAndAction[2].at(0) == 'E') {
            type = CT_END;
        }
        switch (action) {
            case 'F':
                crossingData.push_back(CrossingData(FORWARD, type, crossId));
                break;
            case 'L':
                crossingData.push_back(CrossingData(TURN_LEFT, type, crossId));
                break;
            case 'R':
                crossingData.push_back(CrossingData(TURN_RIGHT, type, crossId));
                break;
        }
    }
    return TrackActionMessage(START, crossingData);
}

TrackActionMessage TrackActionMessage::stop() {
    return TrackActionMessage(STOP);
}
