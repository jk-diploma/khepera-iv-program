//
// Created by jakub on 12/20/20.
//

#include "TrackActionMessageListener.h"
#include "../../utils/Logger.h"
#include "../../communication-architecture/MessageBus.h"
#include "../crossing-actions-holder/CrossingActionsHolderMessage.h"
#include "../robot-drive-loop/RobotDriveLoopMessage.h"
#include "../robot-track-state/RobotTrackStateMessage.h"
#include "../robot-track-state/RobotTrackStateMessageListener.h"

const Void* TrackActionMessageListener::onMessage(TrackActionMessage *message) {
    Logger::info<TrackActionMessageListener>("Arrived message TrackActionMessage");
    MessageBus messageBus = MessageBus::instance();
    if(message->getTrackAction() == START) {
        delete messageBus.sendMessage<RobotTrackStateMessage, RobotTrackState>(RobotTrackStateMessage::start());
        delete messageBus.sendMessage<CrossingActionsHolderMessage, CrossingData>(CrossingActionsHolderMessage::setCrossingActions(message->getCrossingData()));
        messageBus.sendMessage<RobotDriveLoopMessage, Void>(RobotDriveLoopMessage());
    } else {
        delete messageBus.sendMessage<RobotTrackStateMessage, RobotTrackState>(RobotTrackStateMessage::stop());
    }
    return Void::shared();
}
