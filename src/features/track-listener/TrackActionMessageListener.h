//
// Created by jakub on 12/20/20.
//

#ifndef MESSAGE_BUS_LIBRARY_TRACKACTIONMESSAGELISTENER_H
#define MESSAGE_BUS_LIBRARY_TRACKACTIONMESSAGELISTENER_H


#include "../../communication-architecture/listeners/MessageListener.h"
#include "TrackActionMessage.h"
#include "../../communication-architecture/messages/Void.h"

class TrackActionMessageListener: public MessageListener<TrackActionMessage> {
public:
private:
    const Void* onMessage(TrackActionMessage *message);
};


#endif //MESSAGE_BUS_LIBRARY_TRACKACTIONMESSAGELISTENER_H
