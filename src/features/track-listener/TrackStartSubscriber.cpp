//
// Created by jakub on 12/20/20.
//

#include "TrackStartSubscriber.h"
#include "../../mqtt-connection/manager/MqttManager.h"
#include "../../utils/Logger.h"
#include "../../communication-architecture/MessageBus.h"
#include "TrackActionMessage.h"
#include "../../communication-architecture/messages/Void.h"
#include "../../utils/TextSplitter.h"
#include <iostream>

void TrackStartSubscriber::onMessage(TopicPath topic, std::string message) {
    Logger::info<TrackStartSubscriber>("Received start track");
    std::string crossingActions = TextSplitter::split(message, ";")[1];
    MessageBus::instance().sendMessage<TrackActionMessage, Void>(TrackActionMessage::start(crossingActions));
}

std::vector<TopicWildcard> TrackStartSubscriber::topics() {
    std::vector<TopicWildcard> topic;
    topic.push_back(TopicWildcard("/robots/" + MqttManager::getMqttDeviceIdentifier() + "/start-track"));
    return topic;
}
