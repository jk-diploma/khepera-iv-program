//
// Created by jakub on 12/20/20.
//

#ifndef MESSAGE_BUS_LIBRARY_TRACKACTIONMESSAGE_H
#define MESSAGE_BUS_LIBRARY_TRACKACTIONMESSAGE_H


#include <vector>
#include "../../communication-architecture/messages/Message.h"
#include "../../communication-architecture/messages/ReturnData.h"

#define Crossid int

enum CrossingAction {
    FORWARD,
    TURN_RIGHT,
    TURN_LEFT
};

enum CrossingType {
    CT_NONE,
    CT_COLLECT_POINT,
    CT_END,
    CT_START
};

struct CrossingData: public ReturnData {
    CrossingAction action;
    CrossingType type;
    Crossid crossId;

    CrossingData(CrossingAction action, CrossingType type, Crossid crossId): action(action), type(type), crossId(crossId) {}

};

enum TrackAction {
    START,
    STOP
};

class TrackActionMessage: public Message {

    TrackAction action;
    std::vector<CrossingData> crossingData;

    TrackActionMessage(TrackAction action): action(action), crossingData(std::vector<CrossingData>()) {};

    TrackActionMessage(TrackAction action, std::vector<CrossingData> &crossingData): action(action), crossingData(crossingData) {};

public:
    TrackActionMessage() {}
    ~TrackActionMessage() {}

    TrackAction getTrackAction() {
        return action;
    }

    std::vector<CrossingData> getCrossingData() {
        return crossingData;
    }

    static TrackActionMessage stop();

    static TrackActionMessage start(std::string crossingActions);

};


#endif //MESSAGE_BUS_LIBRARY_TRACKACTIONMESSAGE_H
