//
// Created by jakub on 12/20/20.
//

#ifndef MESSAGE_BUS_LIBRARY_TRACKSTOPSUBSCRIBER_H
#define MESSAGE_BUS_LIBRARY_TRACKSTOPSUBSCRIBER_H


#include "../../mqtt-connection/communication/Subscriber.h"

class TrackStopSubscriber: public Subscriber {
    void onMessage(TopicPath topic, std::string message);

    std::vector<TopicWildcard> topics();
};


#endif //MESSAGE_BUS_LIBRARY_TRACKSTOPSUBSCRIBER_H
