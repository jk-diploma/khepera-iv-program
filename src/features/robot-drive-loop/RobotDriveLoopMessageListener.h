//
// Created by jakub on 1/18/21.
//

#ifndef MESSAGE_BUS_LIBRARY_ROBOTDRIVELOOPMESSAGELISTENER_H
#define MESSAGE_BUS_LIBRARY_ROBOTDRIVELOOPMESSAGELISTENER_H


#include "../../communication-architecture/listeners/MessageListener.h"
#include "RobotDriveLoopMessage.h"
#include "../../communication-architecture/messages/Void.h"

class RobotDriveLoopMessageListener: public MessageListener<RobotDriveLoopMessage> {
    bool collisionDetected;

    void onCollisionDetected();

    void onDriving();

public:
    RobotDriveLoopMessageListener(): collisionDetected(false) {};
    const Void *onMessage(RobotDriveLoopMessage *message);

};


#endif //MESSAGE_BUS_LIBRARY_ROBOTDRIVELOOPMESSAGELISTENER_H
