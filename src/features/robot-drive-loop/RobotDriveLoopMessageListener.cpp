//
// Created by jakub on 1/18/21.
//

#include "RobotDriveLoopMessageListener.h"
#include "../../communication-architecture/MessageBus.h"
#include "../detect-collision/DetectCollisionMessage.h"
#include "../line-follower/LineFollowMessage.h"
#include "../robot-track-state/RobotTrackStateMessage.h"
#include "../robot-track-state/RobotTrackStateMessageListener.h"
#include "../detect-collision/DetectCollisionMessageListener.h"
#include "../robot-conflict-started/RobotConflictStartedMessage.h"
#include "../robot-conflict-stopped/RobotConflictStoppedMessage.h"
#include "../robot-state/RobotStateMessageListener.h"
#include <khepera/khepera.h>
#include "../../khepera/Khepera.h"


bool isTrackStarted() {
    const RobotTrackState* trackStartedPointer = MessageBus::instance().sendMessage<RobotTrackStateMessage, RobotTrackState>(RobotTrackStateMessage::check());
    bool trackStarted = trackStartedPointer->isTrackStarted();
    delete trackStartedPointer;
    return trackStarted;
}

const Void* RobotDriveLoopMessageListener::onMessage(RobotDriveLoopMessage *message) {
    Logger::debug<RobotTrackStateMessageListener>("Main robot loop after track started...");
    while(isTrackStarted()) {
        kh4_SetMode(kh4RegSpeedProfile, Khepera::getMicroControllerAccess());
        const CollisionDetectedStatus* collisionDetected
                = MessageBus::instance().sendMessage<DetectCollisionMessage, CollisionDetectedStatus>(DetectCollisionMessage());
        if(collisionDetected->collisionDetected) {
            this->onCollisionDetected();
        } else {
           this->onDriving();
        }
        delete collisionDetected;
    }
    kh4_SetMode(kh4RegIdle, Khepera::getMicroControllerAccess());
    return Void::shared();
}

void RobotDriveLoopMessageListener::onCollisionDetected() {
    Logger::debug<RobotTrackStateMessageListener>("Collision detected. Stopping robot...");
    const RobotStateReturnData* robotState =
            MessageBus::instance().sendMessage<RobotStateMessage, RobotStateReturnData>(RobotStateMessage::getRobotState());
    if(robotState->getRobotState() != SHOULD_DRIVE) {
        if(!this->collisionDetected) {
            MessageBus::instance().sendMessage<RobotConflictStartedMessage, Void>(RobotConflictStartedMessage());
            delete MessageBus::instance().sendMessage<RobotStateMessage, RobotStateReturnData>(RobotStateMessage::setConflicted());
        }
        this->collisionDetected = true;
    } else {
        MessageBus::instance().sendMessage<LineFollowMessage, Void>(LineFollowMessage());
    }
    delete robotState;
}

void RobotDriveLoopMessageListener::onDriving() {
    if(this->collisionDetected) {
        MessageBus::instance().sendMessage<RobotConflictStoppedMessage, Void>(RobotConflictStoppedMessage());
        this->collisionDetected = false;
    }
    Logger::debug<RobotTrackStateMessageListener>("Collision not detected. Following line...");
    delete MessageBus::instance().sendMessage<RobotStateMessage, RobotStateReturnData>(RobotStateMessage::setDriving());
    MessageBus::instance().sendMessage<LineFollowMessage, Void>(LineFollowMessage());
}
