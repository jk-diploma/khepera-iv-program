# Robot drive loop

## What it is for
It is most important feature which is responsible for robot driving logic. It should be alive as long as track to which
robot were assigned is as STARTED state. Setting track as STARTED is responsibility of server.