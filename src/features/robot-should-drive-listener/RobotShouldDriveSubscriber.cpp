//
// Created by user on 3/8/21.
//

#include "RobotShouldDriveSubscriber.h"

#include "../../utils/Logger.h"
#include "../../mqtt-connection/manager/MqttManager.h"
#include "../../communication-architecture/MessageBus.h"
#include "../robot-state/RobotStateMessage.h"
#include "../robot-state/RobotStateMessageListener.h"

void RobotShouldDriveSubscriber::onMessage(TopicPath topic, std::string message) {
    Logger::info<RobotShouldDriveSubscriber>("Robot should drive message received");
    delete MessageBus::instance().sendMessage<RobotStateMessage, RobotStateReturnData>(RobotStateMessage::setShouldDrive());
}

std::vector<TopicWildcard> RobotShouldDriveSubscriber::topics() {
    std::vector<TopicWildcard> topic;
    topic.push_back(TopicWildcard("/robots/" + MqttManager::getMqttDeviceIdentifier() + "/SHOULD_DRIVE"));
    return topic;
}
