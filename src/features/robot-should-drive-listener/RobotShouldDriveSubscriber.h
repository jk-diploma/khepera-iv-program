//
// Created by user on 3/8/21.
//

#ifndef APPLICATION_ROBOTSHOULDDRIVESUBSCRIBER_H
#define APPLICATION_ROBOTSHOULDDRIVESUBSCRIBER_H

#include "../../mqtt-connection/communication/Subscriber.h"

class RobotShouldDriveSubscriber: public Subscriber {

public:

    void onMessage(TopicPath topic, std::string message);

    std::vector<TopicWildcard> topics();

};


#endif //APPLICATION_ROBOTSHOULDDRIVESUBSCRIBER_H
