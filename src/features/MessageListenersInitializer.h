//
// Created by jakub on 1/20/21.
//

#ifndef MESSAGE_BUS_LIBRARY_MESSAGELISTENERSINITIALIZER_H
#define MESSAGE_BUS_LIBRARY_MESSAGELISTENERSINITIALIZER_H


#include "Feature.h"
#include "battery-info/BatteryPercentageMessage.h"
#include "../communication-architecture/listeners/MessageListener.h"
#include "heartbeat/BasicInfoDataMessageListener.h"
#include "track-listener/TrackActionMessage.h"
#include "../mqtt-connection/communication/Subscriber.h"
#include "crossing-actions-holder/CrossingActionsHolderMessage.h"
#include "detect-collision/DetectCollisionMessage.h"
#include "line-follower/LineFollowMessage.h"
#include "robot-drive-loop/RobotDriveLoopMessage.h"
#include "robot-conflict-stopped/RobotConflictStoppedMessage.h"
#include "robot-conflict-started/RobotConflictStartedMessage.h"
#include "robot-track-state/RobotTrackStateMessage.h"
#include "robot-state/RobotStateMessageListener.h"
#include "robot-should-drive-listener/RobotShouldDriveSubscriber.h"

class MessageListenersInitializer: public Feature {
    MessageListener<BatteryPercentageMessage>* batteryMessageListener;
    MessageListener<CrossingActionsHolderMessage>* crossingActionsHolderMessageListener;
    MessageListener<DetectCollisionMessage>* detectCollisionMessageListener;
    MessageListener<BasicInfoDataMessage>* basicInfoDataMessageListener;
    MessageListener<LineFollowMessage>* lineFollowMessageListener;
    MessageListener<RobotDriveLoopMessage>* robotDriveLoopMessageListener;
    MessageListener<RobotConflictStartedMessage>* robotConflictStartedMessageListener;
    MessageListener<RobotConflictStoppedMessage>* robotConflictStoppedMessageListener;
    MessageListener<RobotTrackStateMessage>* robotTrackStateMessageListener;
    MessageListener<TrackActionMessage>* trackActionMessageListener;
    MessageListener<RobotStateMessage>* robotStateMessageListener;

    Subscriber* startTrackSubscriber;
    Subscriber* stopTrackSubscriber;
    Subscriber* robotShouldDriveSubscriber;
public:
    ~MessageListenersInitializer();

    void begin();
};


#endif //MESSAGE_BUS_LIBRARY_MESSAGELISTENERSINITIALIZER_H
