//
// Created by jakub on 12/20/20.
//

#ifndef MESSAGE_BUS_LIBRARY_BATTERYPERCENTAGEDATA_H
#define MESSAGE_BUS_LIBRARY_BATTERYPERCENTAGEDATA_H


#include "../../communication-architecture/messages/ReturnData.h"

struct BatteryPercentageData: ReturnData {

    const int batteryPercentage;

    BatteryPercentageData(int percentage): batteryPercentage((percentage > 100) ? 100 : (percentage < 0 ? 0 : percentage)) {};

};


#endif //MESSAGE_BUS_LIBRARY_BATTERYPERCENTAGEDATA_H
