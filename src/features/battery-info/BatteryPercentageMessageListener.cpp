//
// Created by jakub on 12/20/20.
//

#include "BatteryPercentageMessageListener.h"
#include "../../utils/Logger.h"

#include "../../khepera/Khepera.h"
#include <khepera.h>

const BatteryPercentageData *BatteryPercentageMessageListener::onMessage(BatteryPercentageMessage *message) {
    Logger::debug<BatteryPercentageMessageListener>("Checking battery percentage");
    char buffer[100];
    knet_dev_t* microcontroller = Khepera::getMicroControllerAccess();
    kh4_battery_status(buffer, microcontroller);
    return new BatteryPercentageData((int)buffer[3]);
}
