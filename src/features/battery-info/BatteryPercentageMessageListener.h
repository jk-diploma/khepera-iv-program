//
// Created by jakub on 12/20/20.
//

#ifndef MESSAGE_BUS_LIBRARY_BATTERYPERCENTAGEMESSAGELISTENER_H
#define MESSAGE_BUS_LIBRARY_BATTERYPERCENTAGEMESSAGELISTENER_H


#include "../../communication-architecture/listeners/MessageListener.h"
#include "BatteryPercentageMessage.h"
#include "BatteryPercentageData.h"

class BatteryPercentageMessageListener: public MessageListener<BatteryPercentageMessage> {
public:
    const BatteryPercentageData* onMessage(BatteryPercentageMessage *message);
};


#endif //MESSAGE_BUS_LIBRARY_BATTERYPERCENTAGEMESSAGELISTENER_H
