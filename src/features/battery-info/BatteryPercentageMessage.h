//
// Created by jakub on 12/20/20.
//

#ifndef MESSAGE_BUS_LIBRARY_BATTERYPERCENTAGEMESSAGE_H
#define MESSAGE_BUS_LIBRARY_BATTERYPERCENTAGEMESSAGE_H


#include "../../communication-architecture/messages/Message.h"

struct BatteryPercentageMessage: Message {};


#endif //MESSAGE_BUS_LIBRARY_BATTERYPERCENTAGEMESSAGE_H
