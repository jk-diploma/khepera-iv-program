//
// Created by jakub on 9/29/20.
//

//#include <thread>
#include "ThreadExecutor.h"
#include <unistd.h>

void* start(void* data) {
    ThreadExecutor* executor = (ThreadExecutor*) data;
    pthread_detach(pthread_self());
    usleep(executor->initialDelayInSeconds * 1000000);

    while(executor->alive) {
        Logger::debug<ThreadExecutor>("Executing thread...");
        try {
            executor->thread->execute();
        } catch (std::exception& exception) {
            std::string e = exception.what();
            Logger::error<ThreadExecutor>("Thread end by exception" + e);
        }
        if(executor->callOnce) {
            break;
        }
        usleep(executor->delayBetweenCallsInSeconds * 1000000);
    }
    if(executor->callOnce) {
        executor->stop();
    }
}

void ThreadExecutor::stop() {
    Logger::info<ThreadExecutor>("Ending ThreadExecutor...");
    this->alive = false;
    pthread_exit(NULL);
    delete this;
}

ThreadExecutor *ThreadExecutor::onceThreadCall(BaseThread *thread) {
    return new ThreadExecutor(thread);
}

ThreadExecutor *ThreadExecutor::newScheduledThreadExecutor(
        BaseThread *thread,
        long long int initialDelayInSeconds,
        long long int delayBetweenCallsInSeconds) {
    return new ThreadExecutor(thread, initialDelayInSeconds, delayBetweenCallsInSeconds);
}

ThreadExecutor *ThreadExecutor::execute() {
    pthread_t threadId;
    int rc = pthread_create(&threadId, NULL, start, (void*) this);
    if(rc) {
        Logger::debug<ThreadExecutor>("Thread not created...");
    }
    return this;
}
