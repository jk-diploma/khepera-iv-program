//
// Created by jakub on 9/29/20.
//

#ifndef MESSAGE_BUS_LIBRARY_THREADEXECUTOR_H
#define MESSAGE_BUS_LIBRARY_THREADEXECUTOR_H

#include "BaseThread.h"
#include "../utils/Logger.h"
#include <pthread.h>

#define ll long long

class ThreadExecutor {

    ThreadExecutor(BaseThread *thread, ll initialDelayInSeconds, ll delayBetweenCallsInSeconds)
            : thread(thread),
            initialDelayInSeconds(initialDelayInSeconds),
            delayBetweenCallsInSeconds(delayBetweenCallsInSeconds){
        this->callOnce = false;
        this->alive = true;
    }

    ThreadExecutor(BaseThread *thread)
            : thread(thread), initialDelayInSeconds(0), delayBetweenCallsInSeconds(0) {
        this->callOnce = true;
        this->alive = true;
    }

public:
    const ll initialDelayInSeconds;
    const ll delayBetweenCallsInSeconds;
    bool callOnce;
    bool alive;
    BaseThread *thread;

    ~ThreadExecutor() {
        Logger::info<ThreadExecutor>("Desctructing ThreadExecutor...");
        delete thread;
    }

    ThreadExecutor *execute();

    void stop();

    static ThreadExecutor *newScheduledThreadExecutor(
            BaseThread *thread,
            ll initialDelayInSeconds,
            ll delayBetweenCallsInSeconds);

    static ThreadExecutor *onceThreadCall(BaseThread *thread);
};


#endif //MESSAGE_BUS_LIBRARY_THREADEXECUTOR_H
