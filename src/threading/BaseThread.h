//
// Created by jakub on 9/29/20.
//

#ifndef MESSAGE_BUS_LIBRARY_BASETHREAD_H
#define MESSAGE_BUS_LIBRARY_BASETHREAD_H

class BaseThread {
public:
    virtual void execute() = 0;
};

#endif //MESSAGE_BUS_LIBRARY_BASETHREAD_H
