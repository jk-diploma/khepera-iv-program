//
// Created by jakub on 7/3/20.
//

#include "communication-architecture/listeners/AbstractMessageListener.h"
#include "communication-architecture/listeners/MessageListener.h"
#include "communication-architecture/messages/Message.h"
#include "communication-architecture/messages/Void.h"
#include "threading/BaseThread.h"
#include "threading/ThreadExecutor.h"

#include "mqtt-connection/communication/Subscriber.h"
#include "configs/Properties.h"
#include "configs/Configuration.h"
#include "mqtt-connection/manager/MqttManager.h"
#include "communication-architecture/MessageBus.h"
#include "features/heartbeat/HeartbeatSender.h"
#include "features/MessageListenersInitializer.h"
#include "utils/Converter.h"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>
#include "khepera/Khepera.h"
#include <khepera.h>

#include "utils/Converter.h"

#define MQTT_HOST_KEY "MQTT_HOST"
#define MQTT_PORT_KEY "MQTT_PORT"
#define SEND_DATA_PERIOD_IN_SECONDS_KEY "SEND_DATA_PERIOD_IN_SECONDS"

#define DEFAULT_MQTT_HOST_VALUE "192.168.43.147"
#define DEFAULT_MQTT_PORT_VALUE 9997
#define DEFAULT_SEND_DATA_PERIOD_IN_SECONDS_VALUE 5


class Main {};

void connectWithMqtt();
void listenOnMqttMessages();


int main(int argc, char* argv[]) {
    srand( time( NULL ) );
    Configuration::loadConfigurationPropertiesFromFile(CONFIG_FILE_PATH);
    connectWithMqtt();
    std::vector<Feature*> features;
    features.push_back(new MessageListenersInitializer());
    features.push_back(new HeartbeatSender());
    for(int i = 0; i < features.size(); i++) {
        features[i]->begin();
    }
    listenOnMqttMessages();
    for(int i = 0; i < features.size(); i++) {
        delete features[i];
    }
    features.clear();
    return 0;
}

void connectWithMqtt() {
    Properties properties = Configuration::loadConfigurationPropertiesFromFile(CONFIG_FILE_PATH);
    std::string host = properties.get(MQTT_HOST_KEY, DEFAULT_MQTT_HOST_VALUE);
    int port = properties.get(MQTT_PORT_KEY, DEFAULT_MQTT_PORT_VALUE);
    Logger::info<Main>("MQTT server connection properties are at: tcp://" + host + ":" + toString(port));
    MqttConnectionProperties connectionProperties = MqttConnectionProperties(host, port);
    MqttManager::connect(connectionProperties);
}

void listenOnMqttMessages() {
    MqttManager::getMqttListener().listen();
}
