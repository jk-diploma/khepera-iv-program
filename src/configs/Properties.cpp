//
// Created by jakub on 9/29/20.
//

#include "Properties.h"

void Properties::put(std::string key, std::string value) {
    this->stringProperties.put(key, value);
}

void Properties::put(std::string key, double value) {
    this->doubleProperties.put(key, value);
}

void Properties::put(std::string key, float value) {
    this->floatProperties.put(key, value);
}

void Properties::put(std::string key, int value) {
    this->intProperties.put(key, value);
}


int Properties::get(std::string key, int defaultValue) {
    return intProperties.getValue(key, defaultValue);
}

double Properties::get(std::string key, double defaultValue) {
    return doubleProperties.getValue(key, defaultValue);
}

std::string Properties::get(std::string key, std::string defaultValue) {
    return stringProperties.getValue(key, defaultValue);
}

float Properties::get(std::string key, float defaultValue) {
    return floatProperties.getValue(key, defaultValue);
}
