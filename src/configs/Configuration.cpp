//
// Created by jakub on 9/29/20.
//

#include <fstream>
#include <regex>
#include "Properties.h"
#include "Configuration.h"
#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include <iostream>
#include "../utils/Converter.h"

bool checkRegex(const std::string&, const std::string &regex);
void saveProperty(Properties& properties, const std::string &key, const std::string &value);

//TODO: regex support
const std::string PROPERTY_REGEX = "^([a-zA-Z_]+[a-zA-Z0-9_]*)=([a-zA-Z\\s0-9._]+)$";
const std::string INT_REGEX = "^[0-9]+$";
const std::string FLOATING_REGEX = "^[0-9]+.[0-9]*$";
const std::string DELIMITER = "=";

std::map<std::string, Properties> allProperties;

Properties Configuration::loadConfigurationPropertiesFromFile(const std::string &filePath) {
    if(allProperties.find(filePath) != allProperties.end()) {
        return allProperties[filePath];
    }
    Properties properties = Properties();
    std::ifstream file;
    file.open(filePath.c_str());
    if(file.is_open()) {
        std::smatch sm;
        for(std::string line; getline(file, line);) {
//            std::regex regex(PROPERTY_REGEX);
//            if(std::regex_match(line, sm, regex)) {
                std::string key = line.substr(0, line.find(DELIMITER));
                std::string value = line.substr(line.find(DELIMITER) + 1, line.size());
                std::cout << key << "=" << value << std::endl;
                saveProperty(properties, key, value);
//            }
        }
    }
    file.close();
    allProperties[filePath] = properties;
    return properties;
}

void saveProperty(Properties &properties, const std::string &key, const std::string &value) {
    properties.put(key, value);
}

bool checkRegex(const std::string &value, const std::string &regex) {
    std::smatch sm;
    std::regex reg(regex);
    return std::regex_match(value, sm, reg);
}
