//
// Created by jakub on 9/29/20.
//

#ifndef MESSAGE_BUS_LIBRARY_PROPERTIESHOLDER_H
#define MESSAGE_BUS_LIBRARY_PROPERTIESHOLDER_H

#include <string>
#include <map>

template<typename T>
class PropertiesHolder {
    std::map<std::string, T> properties;
public:
    T getValue(std::string key, T defaultValue);
    void put(std::string key, T value);
};

template<typename T>
T PropertiesHolder<T>::getValue(std::string key, T defaultValue) {
    if(properties.find(key) == properties.end()) {
        properties[key] = defaultValue;
    }
    return properties[key];
}

template<typename T>
void PropertiesHolder<T>::put(std::string key, T value) {
    this->properties[key] = value;
}


#endif //MESSAGE_BUS_LIBRARY_PROPERTIESHOLDER_H
