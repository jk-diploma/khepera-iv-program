//
// Created by jakub on 9/29/20.
//

#ifndef MESSAGE_BUS_LIBRARY_PROPERTIES_H
#define MESSAGE_BUS_LIBRARY_PROPERTIES_H

#include <string>
#include <map>
#include <iostream>
#include "PropertiesHolder.h"

class Properties {
    PropertiesHolder<int> intProperties;
    PropertiesHolder<double> doubleProperties;
    PropertiesHolder<float> floatProperties;
    PropertiesHolder<std::string> stringProperties;

public:

    void put(std::string key, std::string value);
    void put(std::string key, double value);
    void put(std::string key, float value);
    void put(std::string key, int value);

    int get(std::string key, int defaultValue);
    double get(std::string key, double defaultValue);
    std::string get(std::string key, std::string defaultValue);
    float get(std::string key, float defaultValue);

};

#endif //MESSAGE_BUS_LIBRARY_PROPERTIES_H
