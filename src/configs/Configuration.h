//
// Created by jakub on 9/29/20.
//

#ifndef MESSAGE_BUS_LIBRARY_CONFIGURATION_H
#define MESSAGE_BUS_LIBRARY_CONFIGURATION_H

#include "Properties.h"

namespace Configuration {

    Properties loadConfigurationPropertiesFromFile(const std::string &filePath);

}

#endif //MESSAGE_BUS_LIBRARY_CONFIGURATION_H
