//
// Created by jakub on 9/27/20.
//

#ifndef MESSAGE_BUS_LIBRARY_MQTTCONNECTIONPROPERTIES_H
#define MESSAGE_BUS_LIBRARY_MQTTCONNECTIONPROPERTIES_H

#include <string>

struct MqttConnectionProperties {
    std::string address;
    int port;
    MqttConnectionProperties(std::string address, int port): address(address), port(port) {}
};

#endif //MESSAGE_BUS_LIBRARY_MQTTCONNECTIONPROPERTIES_H
