//
// Created by jakub on 9/29/20.
//

#include "MqttListener.h"
#include "../../utils/Logger.h"
#include "../../utils/Converter.h"
#include "MqttManager.h"
#include "../../threading/ThreadExecutor.h"
#include "ThreadSubscriber.h"
#include <iostream>

void MqttListener::listen() const {
    while(connected) {}
}

void MqttListener::messageArrived(std::string topic, std::string message) const {
    Logger::debug<MqttListener>("Message arrived on topic: " + topic + ", msg=\"" + message +  "\"");
    TopicPath topicPath(topic);
    std::vector<Subscriber *> subscribers = MqttManager::getMqttCommunication().getSubscribers();
    for (int i = 0; i < subscribers.size(); i++) {
        Subscriber* subscriber = subscribers[i];
        if(subscriber->matchToTopic(topicPath)) {
            ThreadExecutor::onceThreadCall(new ThreadSubscriber(subscriber, topicPath, message))->execute();
            break;
        }
    }
}
