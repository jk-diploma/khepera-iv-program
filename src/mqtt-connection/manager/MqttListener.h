//
// Created by jakub on 9/29/20.
//

#ifndef MESSAGE_BUS_LIBRARY_MQTTLISTENER_H
#define MESSAGE_BUS_LIBRARY_MQTTLISTENER_H

#include "MQTTClient.h"
#include <iostream>

class MqttListener {
    const bool& connected;
public:
    MqttListener(const bool& connected): connected(connected) {}

    void messageArrived(std::string topic, std::string message) const;

    void listen() const;

};


#endif //MESSAGE_BUS_LIBRARY_MQTTLISTENER_H
