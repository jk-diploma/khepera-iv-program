//
// Created by jakub on 1/18/21.
//

#ifndef MESSAGE_BUS_LIBRARY_THREADSUBSCRIBER_H
#define MESSAGE_BUS_LIBRARY_THREADSUBSCRIBER_H


#include <iostream>
#include "../communication/Subscriber.h"
#include "../../threading/BaseThread.h"
#include "../../threading/ThreadExecutor.h"

class ThreadSubscriber: public BaseThread {
    Subscriber* subscriber;
    TopicPath topicPath;
    std::string payload;
public:
    ThreadSubscriber(Subscriber* subscriber, TopicPath topicPath, std::string payload)
            : subscriber(subscriber), topicPath(topicPath), payload(payload) {};

    void execute() {
        subscriber->onMessage(topicPath, payload);
    }
};


#endif //MESSAGE_BUS_LIBRARY_THREADSUBSCRIBER_H
