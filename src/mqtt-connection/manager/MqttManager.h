//
// Created by jakub on 9/27/20.
//

#ifndef MESSAGE_BUS_LIBRARY_MQTTMANAGER_H
#define MESSAGE_BUS_LIBRARY_MQTTMANAGER_H

#include <string>
#include "../communication/MqttMessage.h"
#include "../communication/Subscriber.h"
#include "MqttConnectionProperties.h"
#include "../communication/MqttCommunication.h"
#include "MqttListener.h"

namespace MqttManager {

    /**
     * Connects with MQTT server. Can be called only once, every next call does nothing.
     * It has reconnecting implemented inside.
     *
     * Defaults listening on subscribers defined in SubscriberInitializer but new subscriber can be add by subscribe method.
     *
     * @param connectionProperties properties of mqtt server
     */
    void connect(MqttConnectionProperties connectionProperties);

    /**
     * Disconnects with MQTT server. It removes all subscribers and disconnect.
     */
    void disconnect();

    /**
     * Retrieves MQTT communication class which has methods to subscribe / publish messages.
     * @return MqttCommunication reference
     */
    MqttCommunication& getMqttCommunication();

    const MqttListener& getMqttListener();

    const std::string &getMqttDeviceIdentifier();

    bool isConnectedWithServer();

}

#endif //MESSAGE_BUS_LIBRARY_MQTTMANAGER_H
