//
// Created by jakub on 9/27/20.
//

#include "MqttManager.h"
#include "../../utils/Logger.h"
#include "../../configs/Configuration.h"
#include "MQTTClient.h"
#include "../../utils/Converter.h"

#ifndef CONFIG_FILE_PATH
#define CONFIG_FILE_PATH "./config.properties"
#endif

#define KEEP_ALIVE_INTERVAL_IN_SECONDS 20
#define CLEAN_SESSION 0
#define ROBOT_IDENTIFIER_KEY "ROBOT_IDENTIFIER"


class MqttManagerNamespace {};

std::string *identifier = NULL;

const std::string fallbackClientUuid = generateSimpleIdentifier();
MqttCommunication* mqttCommunication = NULL;
MqttListener* mqttListener = NULL;
MQTTClient client;
bool isConnected = false;

void delivered(void *context, MQTTClient_deliveryToken dt) {}

int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    std::string topic = toString(topicName);
    char* payload = (char*) message->payload;
    std::string msg = toString(payload);
    Logger::info<MqttManagerNamespace>("New msg arrived: " + msg + ", at topic: " + topic);
    MqttManager::getMqttListener().messageArrived(topic, msg);
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}
void connlost(void *context, char *cause)
{
    isConnected = false;
    Logger::info<MqttManagerNamespace>("Connection with mqtt server lost. Cause: " + toString(cause));
}

std::string prepareMqttServerAddress(MqttConnectionProperties properties);

bool alreadyCallConnect = false;

void MqttManager::connect(MqttConnectionProperties connectionProperties) {
    if(alreadyCallConnect) {
        return ;
    }
    alreadyCallConnect = true;
    disconnect();
    MQTTClient_connectOptions connectOptions = MQTTClient_connectOptions_initializer;
    connectOptions.keepAliveInterval = KEEP_ALIVE_INTERVAL_IN_SECONDS;
    connectOptions.cleansession = CLEAN_SESSION;
    std::string mqttServer = prepareMqttServerAddress(connectionProperties);
    MQTTClient_create(&client, mqttServer.c_str(), getMqttDeviceIdentifier().c_str(), MQTTCLIENT_PERSISTENCE_NONE, NULL);
    MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered);
    if(MQTTClient_connect(client, &connectOptions) != MQTTCLIENT_SUCCESS) {
        isConnected = false;
        Logger::error<MqttManagerNamespace>("Could not connect with mqtt at: " + mqttServer);
    } else {
        isConnected = true;
        Logger::info<MqttManagerNamespace>("Connected with mqtt, client id: " + getMqttDeviceIdentifier());
    }
    mqttCommunication = new MqttCommunication(client, isConnected);
    mqttListener = new MqttListener(isConnected);
}

void MqttManager::disconnect() {
    if (isConnected) {
        //TODO unsubscribe topic
        MQTTClient_disconnect(client, 10000);
        MQTTClient_destroy(&client);
    }
    delete identifier;
}

MqttCommunication& MqttManager::getMqttCommunication() {
    return *mqttCommunication;
}

const MqttListener& MqttManager::getMqttListener() {
    return *mqttListener;
}

const std::string &MqttManager::getMqttDeviceIdentifier() {
    if(identifier == NULL) {
        Properties properties = Configuration::loadConfigurationPropertiesFromFile(CONFIG_FILE_PATH);
        identifier = new std::string(properties.get(ROBOT_IDENTIFIER_KEY, fallbackClientUuid));
    }
    return *identifier;
}

bool MqttManager::isConnectedWithServer() {
    return isConnected;
}
std::string prepareMqttServerAddress(MqttConnectionProperties properties) {
    return "tcp://" + properties.address + ":" + toString(properties.port);
}

