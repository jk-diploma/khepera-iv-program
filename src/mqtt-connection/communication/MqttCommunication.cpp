//
// Created by jakub on 9/29/20.
//

#include "MqttCommunication.h"
#include <iostream>
#include "../../utils/Logger.h"
#include "../../utils/Converter.h"

void MqttCommunication::resubscribe() {
    Logger::info<MqttCommunication>("Resubscribing...", false);
    std::map<std::string, Subscriber*>::iterator it;
    for(it = subscribers.begin(); it != subscribers.end(); it++) {
        subscribe(it->second);
    }
}

Subscriber *MqttCommunication::subscribe(Subscriber *subscriber) {
    subscribers[subscriber->getIdentifier()] = subscriber;
    if(connected) {
        std::vector<TopicWildcard> topics = subscriber->topics();
        for (int i = 0; i < topics.size(); i++) {
            TopicWildcard topic = topics[i];
            Logger::info<MqttCommunication>("Subscribing topic: " + topic.getPath(), false);
            MQTTClient_subscribe(client, topic.getPath().c_str(), topic.getQos());
        }
    }
    return subscriber;
}

void MqttCommunication::unsubscribe(std::string uuid) {
    if(subscribers.find(uuid) != subscribers.end()) {
        delete subscribers[uuid];
    }
    subscribers.erase(uuid);
}

void MqttCommunication::unsubscribe(Subscriber *subscriber) {
    Logger::info<MqttCommunication>("Unsubscribing subscriber: " + subscriber->getIdentifier(), false);
    unsubscribe(subscriber->getIdentifier());
}

void MqttCommunication::publishMessage(MqttMessage *message, std::vector<std::string> topicParams) {
    if(connected) {
        TopicWildcard topic = message->topic();
        std::string topicPath = topic.toTopicPath(topicParams).getPath();
        std::string msg = message->serialize();
        const char* payload = msg.c_str();
        std::cout << "Sending message: " << msg << " to: " << topicPath << std::endl;
        MQTTClient_message pubmsg = MQTTClient_message_initializer;
        pubmsg.payload = (void*) payload;
        pubmsg.payloadlen = msg.size();
        pubmsg.qos = topic.getQos();
        pubmsg.retained = 0;
        MQTTClient_publishMessage(client, topicPath.c_str(), &pubmsg, NULL);
    } else {
        std::cout << "Not connected wit MQTT server... Message not published!" << std::endl;
    }
    delete message;
}

std::vector<Subscriber *> MqttCommunication::getSubscribers() {
    std::vector<Subscriber*> subscribers;
    std::map<std::string, Subscriber*>::iterator it;
    for(it = this->subscribers.begin(); it != this->subscribers.end(); it++) {
        subscribers.push_back(it->second);
    }
    return subscribers;
}

MqttCommunication::~MqttCommunication() {
    std::map<std::string, Subscriber*>::iterator it;
    for(it = subscribers.begin(); it != subscribers.end(); it++) {
        Subscriber* subscriber = it->second;
        unsubscribe(subscriber);
        delete subscriber;
    }
}
