//
// Created by jakub on 9/27/20.
//

#ifndef MESSAGE_BUS_LIBRARY_MQTTMESSAGE_H
#define MESSAGE_BUS_LIBRARY_MQTTMESSAGE_H

#include <string>
#include "../topics/TopicPath.h"

#define Time uint64_t

class MqttMessage {
public:
    virtual std::string serialize() = 0;
    virtual TopicWildcard topic() = 0;
};

#endif //MESSAGE_BUS_LIBRARY_MQTTMESSAGE_H
