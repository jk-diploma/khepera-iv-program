//
// Created by jakub on 9/27/20.
//

#ifndef MESSAGE_BUS_LIBRARY_SUBSCRIBER_H
#define MESSAGE_BUS_LIBRARY_SUBSCRIBER_H

#include <iostream>
#include <string>
#include "../../utils/IdentifierGenerator.h"
#include "../topics/TopicWildcard.h"
#include "../topics/TopicPath.h"

class Subscriber {
    const std::string *identifier;
public:

    Subscriber(): identifier(NULL) {}

    virtual ~Subscriber() {
        delete identifier;
    }

    virtual void onMessage(TopicPath topic, std::string message){}

    virtual std::vector<TopicWildcard> topics() = 0;

    virtual bool matchToTopic(TopicPath topic) {
        std::vector<TopicWildcard> subscriberTopics = topics();
        for(int i = 0; i < subscriberTopics.size(); i++) {
            TopicWildcard subscriberTopic = subscriberTopics[i];
            std::cout << "Topic path: " << topic.getPath() << ", subscriber path: " << subscriberTopic.getPath() << std::endl;
            if(topic.match(subscriberTopic)) {
                return true;
            }
        }
        return false;
    }

    virtual std::string getIdentifier() {
        if(identifier == NULL) {
            identifier = new std::string(generateSimpleIdentifier());
        }
        return *identifier;
    }
};

#endif //MESSAGE_BUS_LIBRARY_SUBSCRIBER_H
