//
// Created by jakub on 9/29/20.
//

#ifndef MESSAGE_BUS_LIBRARY_MQTTCOMMUNICATION_H
#define MESSAGE_BUS_LIBRARY_MQTTCOMMUNICATION_H

#include <string>
#include <vector>
#include <map>
#include "MQTTClient.h"
#include "Subscriber.h"
#include "MqttMessage.h"

class MqttCommunication {
private:
    MQTTClient& client;
    const bool& connected;
    std::map<std::string, Subscriber*> subscribers;
public:

    MqttCommunication(
            MQTTClient& client,
            const bool& connected): client(client), connected(connected) {}

    ~MqttCommunication();

    void resubscribe();

    Subscriber* subscribe(Subscriber* subscriber);

    void unsubscribe(Subscriber* subscriber);

    void unsubscribe(std::string uuid);

    /**
     * Publish given MqttMessage. It sends message with given params to defined topic in message class.
     *
     * <b>At the end it deletes pointer so make sure that You wouldn't use <i>message</i> after calling this method.</b>
     * @param message message to send to another client listening on given topic in <i>message</i>
     * @param topicParams params to defined topic. It should be given in same order as topic. For example topic:
     * <p><i>/robots/<b>+</b>/crossId/<b>+</b>/stopped</br></p>
     * <p>Element at <i>topicParams[0]</i> is equal to first <i>+</i></br></p>
     * <p>Element at <i>topicParams[1]</i> is equal to second <i>+</i></br></p>
     *
     */
    void publishMessage(MqttMessage* message, std::vector<std::string> topicParams = std::vector<std::string>());

    std::vector<Subscriber*> getSubscribers();

};


#endif //MESSAGE_BUS_LIBRARY_MQTTCOMMUNICATION_H
