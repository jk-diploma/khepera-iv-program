//
// Created by jakub on 9/28/20.
//

#include "TopicPath.h"
#include "TopicWildcard.h"
#include "TopicNotValidException.h"
#include "TopicConstants.h"
#include <regex>
#include "../../utils/TextSplitter.h"

TopicPath::TopicPath(std::string path, int qos) {
    if(isValid(path)) {
        this->path = path;
        this->qos = qos;
    } else {
        throw TopicNotValidException(path, TOPIC_PATH_REGEX);
    }
}

//FIXME
bool TopicPath::isValid(std::string path) {
    return true;
//    std::smatch sm;
//    std::regex regex(TOPIC_PATH_REGEX);
//    return std::regex_match(path, sm, regex);
}

bool TopicPath::match(TopicWildcard topicWildcard) {
    std::vector<std::string> incomingMessageTopic = TextSplitter::split(this->path, "/");
    std::vector<std::string> subscriberTopic = TextSplitter::split(topicWildcard.toTopicRegex(), "/");
    if(incomingMessageTopic.size() == subscriberTopic.size()) {
        for(int i = 0; i < incomingMessageTopic.size(); i++) {
            std::string msgPart = incomingMessageTopic[i];
            std::string subPart = subscriberTopic[i];
            if(subPart != "." && subPart != msgPart) {
                return false;
            }
        }
        return true;
    }
    return false;
    //    std::regex regex = topicWildcard.toTopicRegex();
//    std::smatch sm;
//    return std::regex_match(this->path, sm, regex);
}

std::string TopicPath::getPath() {
    return this->path;
}

int TopicPath::getQos() {
    return this->qos;
}
