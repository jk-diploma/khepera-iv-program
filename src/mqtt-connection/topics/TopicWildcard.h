//
// Created by jakub on 9/28/20.
//

#ifndef MESSAGE_BUS_LIBRARY_TOPICWILDCARD_H
#define MESSAGE_BUS_LIBRARY_TOPICWILDCARD_H

class TopicPath;

#include <string>
#include "TopicRegex.h"

class TopicWildcard {
    std::string path;
    int qos;

    bool isValid(std::string path);

public:

    TopicWildcard(std::string path, int qos = 0);

    std::string& getPath();

    int getQos();

    std::string toTopicRegex();

    TopicPath toTopicPath(std::vector<std::string> params);
};


#endif //MESSAGE_BUS_LIBRARY_TOPICWILDCARD_H
