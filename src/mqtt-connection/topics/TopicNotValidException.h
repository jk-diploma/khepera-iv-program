//
// Created by jakub on 9/28/20.
//

#ifndef MESSAGE_BUS_LIBRARY_TOPICNOTVALIDEXCEPTION_H
#define MESSAGE_BUS_LIBRARY_TOPICNOTVALIDEXCEPTION_H

#include <exception>
#include <string>
#include "../../utils/Logger.h"

class TopicNotValidException: std::exception {
    std::string value;
    std::string regexPattern;
public:
    ~TopicNotValidException() throw() {}

    TopicNotValidException(std::string value, std::string regexPattern) throw(): value(value), regexPattern(regexPattern)  {
        Logger::error<TopicNotValidException>("Topic not valid. Topic: " + value + ", regex " + regexPattern);
    }

    const char* what() const throw() {
        std::string message = "Topic does not match with pattern: " + regexPattern;
        return message.c_str();
    }
};

#endif //MESSAGE_BUS_LIBRARY_TOPICNOTVALIDEXCEPTION_H
