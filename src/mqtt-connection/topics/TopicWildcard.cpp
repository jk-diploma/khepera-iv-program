//
// Created by jakub on 9/28/20.
//

#include "TopicWildcard.h"
#include "TopicPath.h"
#include "TopicNotValidException.h"
#include "TopicConstants.h"
#include <tr1/regex>
#include "../../utils/TextSplitter.h"

TopicWildcard::TopicWildcard(std::string path, int qos) {
    if(isValid(path)) {
        this->path = path;
        this->qos = qos;
    } else {
        throw TopicNotValidException(path, TOPIC_WITH_WILDCARD_REGEX);
    }
}

//FIXME
bool TopicWildcard::isValid(std::string path) {
    return true;
//    std::cout << "Topic: " << path << " validator: " << TOPIC_WITH_WILDCARD_REGEX << std::endl;
//    std::string r = TOPIC_WITH_WILDCARD_REGEX;
//    std::smatch sm;
//    std::regex regex(r);
//    return std::regex_match(path, sm, regex);
}


std::string TopicWildcard::toTopicRegex() {
    std::string newTopic;
    std::vector<std::string> parts = TextSplitter::split2(path, '/');
    for(int i = 0; i < parts.size(); i++) {
        std::string part = parts[i];
        if(part == "+" || part == "#") {
            newTopic = newTopic + "/.";
            std::cout << newTopic << std::endl;
        } else {
            newTopic = newTopic + "/" + part;
            std::cout << newTopic << std::endl;
        }
    }
    return newTopic;
//    std::regex wildcards(TOPIC_WILDCARDS_REGEX);
//    std::string wildcardAsRegex(TOPIC_WILDCARD_REGEX_REPRESENTATION);
//    return TopicRegex(std::regex_replace(path, wildcards, wildcardAsRegex));
}

std::string& TopicWildcard::getPath() {
    return this->path;
}

int TopicWildcard::getQos() {
    return this->qos;
}

TopicPath TopicWildcard::toTopicPath(std::vector<std::string> params) {
    std::string topicPath = std::string();
    int index = 0;
    for(int i = 0; i < path.size(); i++) {
        char c = path[i];
        if(c == '#' || c == '+') {
            if(index < params.size()) {
                topicPath.append(params[index++]);
            } else {
                //FIXME throw exception
            }
        } else {
            topicPath.append(1, c);
        }
    }
    return TopicPath(topicPath, qos);
}
