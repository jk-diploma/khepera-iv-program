//
// Created by jakub on 9/28/20.
//

#ifndef MESSAGE_BUS_LIBRARY_TOPICPATH_H
#define MESSAGE_BUS_LIBRARY_TOPICPATH_H

#include <string>
#include "TopicWildcard.h"

class TopicPath {
    std::string path;
    int qos;

    bool isValid(std::string path);

public:
    TopicPath(std::string path, int qos = 0);

    std::string getPath();

    int getQos();

    bool match(TopicWildcard topicWildcard);
};


#endif //MESSAGE_BUS_LIBRARY_TOPICPATH_H
