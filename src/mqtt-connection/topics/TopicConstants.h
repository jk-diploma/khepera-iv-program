//
// Created by jakub on 9/28/20.
//

#ifndef MESSAGE_BUS_LIBRARY_TOPICCONSTANTS_H
#define MESSAGE_BUS_LIBRARY_TOPICCONSTANTS_H

#include <string>

const std::string TOPIC_WITH_WILDCARD_REGEX("[a-zA-Z0-9\\/+#\\-_]+");
const std::string TOPIC_PATH_REGEX("[a-zA-Z0-9/\\-_]+");
const std::string TOPIC_WILDCARDS_REGEX("[\\+#]+");
const std::string TOPIC_WILDCARD_REGEX_REPRESENTATION("[a-zA-Z0-9\\-_]*"); //TODO check if shouldn't be replaced with +

#endif //MESSAGE_BUS_LIBRARY_TOPICCONSTANTS_H
