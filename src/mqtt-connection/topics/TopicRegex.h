//
// Created by jakub on 9/28/20.
//

#ifndef MESSAGE_BUS_LIBRARY_TOPICREGEX_H
#define MESSAGE_BUS_LIBRARY_TOPICREGEX_H

#include <regex>
#include <string>

class TopicRegex {
    std::string topicAsRegex;
public:
    TopicRegex(std::string topicAsRegex): topicAsRegex(topicAsRegex) {}

    std::regex regex();
};


#endif //MESSAGE_BUS_LIBRARY_TOPICREGEX_H
