# Khepera IV program

## 1. Description
This is program running on Khepera IV robots. But because of cross compilation it is possible to run program
at computer. Minimal requirements is Linux OS as prepare application script is wrote using bash. If You have
Windows or Mac You should manually include required libraries which You can find in CMakeLists.txt.
This program is responsible for listening at MQTT messages on specific topics. Also it has endless thread which
is sending heartbeat to topic: `/robots/{robotIdentifier}/heartbeat`.
## 2. Configuring
Application configuration is done by two ways: before compiling application, before running application.
1. Before compiling
    1. Open `CMakeLists.txt`
    1. You can edit two variables:
        ```cmake
        set(CROSS_COMPILING false)
        set(COMPILER_DEBUG true)
        ```
        CROSS_COMPILING set as true compiles application at khepera IV (by using its toolchain), 
        set as false will be compiled by default compiler
        
        COMPILER_DEBUG adds additional output at compiling
1. Before running application
    1. You can place file with name `config.properties` at root directory (at same level as program) and edit:
    ```
   MQTT_HOST=172.17.0.1
   MQTT_PORT=9997
   SEND_DATA_PERIOD_IN_SECONDS=15
   ROBOT_IDENTIFIER=HELLO_WORLD_3
   LOG_LEVEL=INFO
   ```
   Where:
   
   MQTT_HOST => is ip address or domain of MQTT server
   
   MQTT_PORT => is port of MQTT server
   
   SEND_DATA_PERIOD_IN_SECONDS => frequency of heartbeats. Every defined seconds robot will send heartbeat at MQTT topic
   
   ROBOT_IDENTIFIER => will be used to display identifier at server. It should be unique across every robot connected 
   to MQTT. It can be whatever which will be helpful to identify robot (for example You can stick green band and name
   robot GREEN_ROBOT)
   
   LOG_LEVEL => NOT SUPPORTED YET 
## 3. Running
1. Go into project root directory
1. Execute command: 
    ```sh 
    bash ./prepare_application.sh
    ```
    And wait for end
1. You can now run application by IDE or by executing commands
    ```shell script
    cmake .
    cd build
    make
    ./message_bus_library
    ```